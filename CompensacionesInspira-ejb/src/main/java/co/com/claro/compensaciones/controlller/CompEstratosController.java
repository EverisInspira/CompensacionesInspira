/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.controlller;

import co.com.claro.compensaciones.dao.CompEstratosDao;
import co.com.claro.compensaciones.entity.CompEstratos;
import co.com.claro.compensaciones.util.EJBUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author candradh
 */
@Stateless
public class CompEstratosController {

    @PersistenceContext(unitName = "gestionnew")
    private EntityManager entityManager;

    /**
     * Objetivo:metodo realiza la conversion de lista tipo CompEstratos a
     * CompEstratosDao
     *
     * @author candradh-everis
     * @version 05/09/2018
     * @param listEntity - Lista de objetos tipo CompEstratos
     * @retunr - lista de objetos tipo CompEstratosDao
     * @throws java.lang.Exception
     */
    private List<CompEstratosDao> convertToDao(List<CompEstratos> listEntity){
        List<CompEstratosDao> listDao = new ArrayList<>();    
        try {
            for (CompEstratos compEstratos : listEntity) {
                CompEstratosDao compEstratosDao
                        = new CompEstratosDao();

                    compEstratosDao = (CompEstratosDao) EJBUtils.
                            mapperEntityAndDao(compEstratosDao, compEstratos);

                listDao.add(compEstratosDao);
            }
         } catch (Exception ex) {
                Logger.getLogger(CompEstratosController.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        return listDao;        
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Descripcion: mapea la tabla de base de datos de COMP_AJUSTES
 *
 * @author vcastril-@everis
 * @version 04/09/2018
 */
@Entity
@Table(name = "COMP_AJUSTES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompAjustes.findAll", 
            query = "SELECT c FROM CompAjustes c"),
    @NamedQuery(name = "CompAjustes.findByIdAjuste",
            query = "SELECT c FROM CompAjustes c WHERE c.idAjuste = :idAjuste"),
    @NamedQuery(name = "CompAjustes.findByCodAjuste", 
            query = "SELECT c FROM CompAjustes c "
                    + "WHERE c.codAjuste = :codAjuste"),
    @NamedQuery(name = "CompAjustes.findByDescripcion", 
            query = "SELECT c FROM CompAjustes c "
                    + "WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CompAjustes.findByEstado", 
            query = "SELECT c FROM CompAjustes c "
                    + "WHERE c.estado = :estado")})
@SequenceGenerator(name="idAjuste", sequenceName="seq_COMP_AJUSTES",
            initialValue=1, allocationSize=1) 
public class CompAjustes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_AJUSTE")
    @GeneratedValue(generator = "idAjuste", 
            strategy = GenerationType.SEQUENCE)
    private Long idAjuste;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "COD_AJUSTE")
    private String codAjuste;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Long estado;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "USUARIO")
    private String usuario;

    public CompAjustes() {
    }

    public CompAjustes(Long idAjuste) {
        this.idAjuste = idAjuste;
    }

    public CompAjustes(Long idAjuste, String codAjuste,
            String descripcion, Long estado) {
        this.idAjuste = idAjuste;
        this.codAjuste = codAjuste;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public Long getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(Long idAjuste) {
        this.idAjuste = idAjuste;
    }

    public String getCodAjuste() {
        return codAjuste;
    }

    public void setCodAjuste(String codAjuste) {
        this.codAjuste = codAjuste;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAjuste != null ? idAjuste.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CompAjustes)) {
            return false;
        }
        CompAjustes other = (CompAjustes) object;
        if ((this.idAjuste == null && other.idAjuste != null) ||
                (this.idAjuste != null && !this.idAjuste.equals(other.idAjuste))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.ces3.rest.CompAjustes[ idAjuste=" + idAjuste + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vcastril
 */
@Entity
@Table(name = "COMP_CAUSAS_AUD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompCausasAud.findAll", query = "SELECT c FROM CompCausasAud c"),
    @NamedQuery(name = "CompCausasAud.findById", query = "SELECT c FROM CompCausasAud c WHERE c.id = :id"),
    @NamedQuery(name = "CompCausasAud.findByIdCausa", query = "SELECT c FROM CompCausasAud c WHERE c.idCausa = :idCausa"),    
    @NamedQuery(name = "CompCausasAud.findByDescripcion", query = "SELECT c FROM CompCausasAud c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CompCausasAud.findByCodCausa", query = "SELECT c FROM CompCausasAud c WHERE c.codCausa = :codCausa"),
    @NamedQuery(name = "CompCausasAud.findByEstado", query = "SELECT c FROM CompCausasAud c WHERE c.estado = :estado"),
    @NamedQuery(name = "CompCausasAud.findByUsuarioModifico", query = "SELECT c FROM CompCausasAud c WHERE c.usuarioModifico = :usuarioModifico"),
    @NamedQuery(name = "CompCausasAud.findByOperacion", query = "SELECT c FROM CompCausasAud c WHERE c.operacion = :operacion"),
    @NamedQuery(name = "CompCausasAud.findByIp", query = "SELECT c FROM CompCausasAud c WHERE c.ip = :ip")})


@SequenceGenerator(name="id", sequenceName="COMP_CAUSAS_AUD_SEQ", 
            initialValue=1, allocationSize=1)
public class CompCausasAud implements Serializable {

    private static final long serialVersionUID = 1L;
   @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(generator = "id",
            strategy= GenerationType.SEQUENCE)
    private Long id;   
    @Basic(optional = false)
    @Column(name = "ID_CAUSA")
    private Long idCausa;   
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "COD_CAUSA")
    private String codCausa;
    @Basic(optional = false)
    @Column(name = "ESTADO")
    private Long estado;
    @Column(name = "USUARIO_MODIFICO")
    private String usuarioModifico;
    @Column(name = "OPERACION")
    private String operacion;
    @Column(name = "IP")
    private String ip;

    public CompCausasAud() {
    }

    public CompCausasAud(Long id) {
        this.id = id;
    }

    public CompCausasAud(Long id, Long idCausa, String descripcion,
            String codCausa, Long estado) {
        this.id = id;
        this.idCausa = idCausa;        
        this.descripcion = descripcion;
        this.codCausa = codCausa;
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCausa() {
        return idCausa;
    }

    public void setIdCausa(Long idCausa) {
        this.idCausa = idCausa;
    }   

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodCausa() {
        return codCausa;
    }

    public void setCodCausa(String codCausa) {
        this.codCausa = codCausa;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getUsuarioModifico() {
        return usuarioModifico;
    }

    public void setUsuarioModifico(String usuarioModifico) {
        this.usuarioModifico = usuarioModifico;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CompCausasAud)) {
            return false;
        }
        CompCausasAud other = (CompCausasAud) object;
        if ((this.id == null && other.id != null) || 
                (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.CompCausasAud[ id=" + id + " ]";
    }
    
}

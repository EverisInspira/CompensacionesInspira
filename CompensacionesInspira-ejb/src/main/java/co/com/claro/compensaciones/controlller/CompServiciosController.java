/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.controlller;

import co.com.claro.compensaciones.dao.CompServiciosDao;
import co.com.claro.compensaciones.entity.CompServicios;
import co.com.claro.compensaciones.util.EJBUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author candradh
 */
@Stateless
public class CompServiciosController {

    @PersistenceContext(unitName = "gestionnew")
    private EntityManager entityManager;

    /**
     * Objetivo:metodo realiza la conversion de lista tipo CompServicios a
     * CompServiciosDao
     *
     * @author candradh-everis
     * @version 05/09/2018
     * @param listEntity - Lista de objetos tipo CompServicios
     * @retunr - lista de objetos tipo CompServiciosDao
     * @throws java.lang.Exception
     */
    private List<CompServiciosDao> convertToDao(List<CompServicios> listEntity){
        List<CompServiciosDao> listDao = new ArrayList<>();   
        try {
            for (CompServicios compServicios : listEntity) {
                CompServiciosDao compServiciosDao
                        = new CompServiciosDao();

                    compServiciosDao = (CompServiciosDao) EJBUtils.
                            mapperEntityAndDao(compServiciosDao, compServicios);

                listDao.add(compServiciosDao);
            }
        } catch (Exception ex) {
                Logger.getLogger(CompServiciosController.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        return listDao;      
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}

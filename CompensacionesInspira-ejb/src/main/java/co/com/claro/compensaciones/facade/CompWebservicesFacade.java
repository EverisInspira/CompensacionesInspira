/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.facade;

import co.com.claro.compensaciones.controlller.CompWebServicesController;
import co.com.claro.compensaciones.dao.CompWebServicesDao;
import co.com.claro.compensaciones.facadelocal.ICompWebservicesFacadeLocal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Descripcion: clase que implementa la interfaz ICompWebservicesFacadeLocal e
 * interactua con la entidad CompWebservicesFacade Objetivo: consultar todos los
 * datos de la tabla COMP_WEB_SERVICES.
 *
 * @author Carlos Adrian Andrade
 * @version 04/09/2018
 */
@Stateless
public class CompWebservicesFacade implements ICompWebservicesFacadeLocal {

    @EJB
    CompWebServicesController compWebServicesController;

    /**
     * Objetivo: listar datos de tipo CompWebServicesDao
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param descripcion - descripcion que identifica al servicio web
     * @param estado - estado del servicio web
     * @return - Lista de objetos tipo CompWebServicesDao
     * @throws Exception
     */
    @Override
    public List<CompWebServicesDao> findAll(String descripcion, Long estado)
            throws Exception {
        try {
            return compWebServicesController.findAll(descripcion, estado);
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
            return null;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompAjustes;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Descripcion: Objeto de transferencia de datos de la tabla Comp_Ajustes_Aud
 *
 * @author candradh-@everis
 * @version 10/09/2018
 */
public class CompAjustesAudDao extends AbstractDao<CompAjustes> {
    
    private Long id;    
    private Long idAjuste;    
    private String descripcion;   
    private String codAjuste;    
    private Long estado;    
    private String usuarioModifico;    
    private String operacion;    
    private String ip;
    
    
    @PersistenceContext
    private EntityManager entityManager;

    public CompAjustesAudDao() {
        super(CompAjustes.class);
    }

    public CompAjustesAudDao(EntityManager entityManager) {
        super(CompAjustes.class);
        this.entityManager = entityManager;
    }


    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @return the idAjuste
     */
    public Long getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(Long idAjuste) {
        this.idAjuste = idAjuste;
    }

    public String getCodAjuste() {
        return codAjuste;
    }

    public void setCodAjuste(String codAjuste) {
        this.codAjuste = codAjuste;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }    

    /**
     * @return the usuarioModifico
     */
    public String getUsuarioModifico() {
        return usuarioModifico;
    }

    /**
     * @param usuarioModifico the usuarioModifico to set
     */
    public void setUsuarioModifico(String usuarioModifico) {
        this.usuarioModifico = usuarioModifico;
    }

    /**
     * @return the operacion
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * @param operacion the operacion to set
     */
    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
    
    
}

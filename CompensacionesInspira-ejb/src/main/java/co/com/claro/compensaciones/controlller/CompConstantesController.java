/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.controlller;

import co.com.claro.compensaciones.dao.CompConstantesDao;
import co.com.claro.compensaciones.entity.CompConstantes;
import co.com.claro.compensaciones.util.EJBUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author candradh
 */
@Stateless
public class CompConstantesController {

    @PersistenceContext(unitName = "gestionnew")
    private EntityManager entityManager;

    /**
     * Objetivo:metodo realiza la conversion de lista tipo CompConstantes a 
     * CompConstantesDao
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param listEntity - Lista de objetos tipo CompConstantes
     * @retunr - lista de objetos tipo CompConstantesDao
     * @throws java.lang.Exception
     */
    private List<CompConstantesDao> convertToDao(List<CompConstantes> listEntity){
        List<CompConstantesDao> listDao = new ArrayList<>();   
        try {  
            for (CompConstantes compWebservices : listEntity) {
                CompConstantesDao compWebServicesDao
                        = new CompConstantesDao();

                    compWebServicesDao = (CompConstantesDao) EJBUtils.
                            mapperEntityAndDao(compWebServicesDao, compWebservices);

                listDao.add(compWebServicesDao);
            }
        } catch (Exception ex) {
                Logger.getLogger(CompConstantesController.class.getName()).
                        log(Level.SEVERE, null, ex);
        }
        return listDao;       
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.controlller;

import co.com.claro.compensaciones.dao.CompCausasDao;
import co.com.claro.compensaciones.entity.CompCausas;
import co.com.claro.compensaciones.util.EJBUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica
 * Descripción: Clase que administra los métodos correspondientes para la
 * fábrica
 *
 * @author Carlos Andrade - everis
 * @version 04/09/2018
 */
@Stateless
public class CompCausasController {

    @PersistenceContext(unitName = "gestionnew")
    private EntityManager entityManager;

    /**
     * Objetivo:metodo  para la insercion sobre la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausasDao - Objeto que se quiere almacenar    
     */
    public void create(CompCausasDao compCausasDao){
        CompCausas causas = convertEntity(compCausasDao);
        this.getEntityManager().persist(causas);
    }
    
    /**
     * Objetivo:metodo  para la actualizacion sobre la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustesDao - Objeto que se quiere actualizar  
     */
    public void edit(CompCausasDao compAjustesDao){
        CompCausas causas = convertEntity(compAjustesDao);
        this.getEntityManager().merge(causas);
    }

    /**
     * Objetivo:metodo para la eliminación de la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustesDao - Objeto que se quiere eliminar     
     */
    public void remove(CompCausasDao compAjustesDao){
        CompCausas causas = convertEntity(compAjustesDao);
        this.getEntityManager().remove(getEntityManager().
                merge(causas));
        
    }
    
     /**
     * Objetivo: listar datos de tipo CompAjustesDao
     *
     * @author vcastril-everis
     * @version 04/09/2018
     * @return - Lista de objetos tipo CompAjustesDao
     * @throws Exception
     */
    public List<CompCausasDao> findAll()
            throws Exception {
        List<CompCausas> list = this.getEntityManager()
                .createNamedQuery("CompCausas.findAll")
                .getResultList();
        return convertToDao(list);
    }
    
    public List<CompCausasDao>  findDescripcion(String descripcion)
            throws Exception {
         List<CompCausas> list = this.getEntityManager()
                .createNamedQuery("CompCausas.findByDescrpcion")
                 .setParameter("descrpcion", descripcion)
                .getResultList();
        return convertToDao(list);
    }

    public List<CompCausasDao> findCodigo(String codCausa) throws Exception {
        List<CompCausas> list = this.getEntityManager()
                .createNamedQuery("CompCausas.findByCodCausa")
                 .setParameter("codCausa", codCausa)
                .getResultList();
        return convertToDao(list);
    }
    /**
     * Objetivo:metodo realiza la conversion de lista tipo CompCausas a
     * CompCausasDao
     *
     * @author candradh-everis
     * @version 05/09/2018
     * @param listEntity - Lista de objetos tipo CompCausas
     * @return - lista de objetos tipo CompCausasDao
     * @throws java.lang.Exception
     */
    private List<CompCausasDao> convertToDao(List<CompCausas> listEntity){
        List<CompCausasDao> listDao = new ArrayList<>();
        try {
            for (CompCausas compCausas : listEntity) {
                CompCausasDao compCausasDao
                        = new CompCausasDao();
                compCausasDao = (CompCausasDao) EJBUtils.
                        mapperEntityAndDao(compCausasDao, compCausas);
                listDao.add(compCausasDao);
            }
            
        } catch (Exception ex) {
            Logger.getLogger(CompWebServicesController.class.getName())
                    .log(Level.SEVERE, null,ex);
        }
        return listDao;
    }

    /**
     * Objetivo:metodo realiza la conversion de tipo CompCausas a
     * CompCausasDao
     *
     * @author candradh-everis
     * @version 05/09/2018
     * @param listEntity - Lista de objetos tipo CompCausas
     * @return - lista de objetos tipo CompCausasDao
     * @throws java.lang.Exception
     */
    private CompCausas convertEntity(CompCausasDao compCausasDao){
        CompCausas causas = new CompCausas();
        try {
            causas = (CompCausas)
                    EJBUtils.mapperEntityAndDao(causas, compCausasDao);
        } catch (Exception ex) {
            Logger.getLogger(CompCausasController.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return causas;
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}

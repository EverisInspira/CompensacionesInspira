package co.com.claro.compensaciones.facadelocal;

import co.com.claro.compensaciones.dao.CompWebServicesDao;
import java.util.List;
import javax.ejb.Local;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica
 * Descripción: Clase que administra los métodos correspondientes para la
 * fábrica
 *
 * @author Carlos Andrade - everis
 * @version 04/09/2018
 */
@Local
public interface ICompWebservicesFacadeLocal {

    /**
     * Objetivo: listar datos de tipo CompWebServicesDao
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param descripcion - descripcion que identifica al servicio web
     * @param estado - estado del servicio web
     * @return - Lista de objetos tipo CompWebServicesDao
     * @throws Exception
     */
    public List<CompWebServicesDao> findAll(String descripcion, Long estado)
            throws Exception;

}

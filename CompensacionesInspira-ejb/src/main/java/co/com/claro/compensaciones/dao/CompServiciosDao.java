/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompServicios;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author candradh
 */
public class CompServiciosDao extends AbstractDao<CompServicios> {
    
     private Float idServicio;   
    private String nombreServicio;   
    private String codServicio;   
    private String descripcion;
    
    
    @PersistenceContext
    private EntityManager entityManager;

    public CompServiciosDao() {
        super(CompServicios.class);
    }

    public CompServiciosDao(EntityManager entityManager) {
        super(CompServicios.class);
        this.entityManager = entityManager;
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @return the idServicio
     */
    public Float getIdServicio() {
        return idServicio;
    }

    /**
     * @param idServicio the idServicio to set
     */
    public void setIdServicio(Float idServicio) {
        this.idServicio = idServicio;
    }

    /**
     * @return the nombreServicio
     */
    public String getNombreServicio() {
        return nombreServicio;
    }

    /**
     * @param nombreServicio the nombreServicio to set
     */
    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    /**
     * @return the codServicio
     */
    public String getCodServicio() {
        return codServicio;
    }

    /**
     * @param codServicio the codServicio to set
     */
    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

  

   
    
    
}

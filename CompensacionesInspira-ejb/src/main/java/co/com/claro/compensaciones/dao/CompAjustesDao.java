/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompAjustes;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Descripcion: Objeto de transferencia de datos de la tabla COMP_AJUSTES
 *
 * @author vcastril-@everis
 * @version 04/09/2018
 */
public class CompAjustesDao extends AbstractDao<CompAjustes> 
        implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private Long idAjuste;    
    private String codAjuste;   
    private String descripcion;   
    private Long estado;
    private String usuario;
    
    @PersistenceContext
    private EntityManager entityManager;

    public CompAjustesDao() {
        super(CompAjustes.class);
    }

    public CompAjustesDao(EntityManager entityManager) {
        super(CompAjustes.class);
        this.entityManager = entityManager;
    }


    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @return the idAjuste
     */
    public Long getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(Long idAjuste) {
        this.idAjuste = idAjuste;
    }

    public String getCodAjuste() {
        return codAjuste;
    }

    public void setCodAjuste(String codAjuste) {
        this.codAjuste = codAjuste;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
    
}

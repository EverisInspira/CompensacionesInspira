/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vcastril
 */
@Entity
@Table(name = "COMP_AJUSTES_AUD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompAjustesAud.findAll", query = "SELECT c FROM CompAjustesAud c"),
    @NamedQuery(name = "CompAjustesAud.findById", query = "SELECT c FROM CompAjustesAud c WHERE c.id = :id"),
    @NamedQuery(name = "CompAjustesAud.findByIdAjuste", query = "SELECT c FROM CompAjustesAud c WHERE c.idAjuste = :idAjuste"),    
    @NamedQuery(name = "CompAjustesAud.findByDescripcion", query = "SELECT c FROM CompAjustesAud c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "CompAjustesAud.findByCodAjuste", query = "SELECT c FROM CompAjustesAud c WHERE c.codAjuste = :codAjuste"),
    @NamedQuery(name = "CompAjustesAud.findByEstado", query = "SELECT c FROM CompAjustesAud c WHERE c.estado = :estado"),
    @NamedQuery(name = "CompAjustesAud.findByUsuarioModifico", query = "SELECT c FROM CompAjustesAud c WHERE c.usuarioModifico = :usuarioModifico"),
    @NamedQuery(name = "CompAjustesAud.findByOperacion", query = "SELECT c FROM CompAjustesAud c WHERE c.operacion = :operacion"),
    @NamedQuery(name = "CompAjustesAud.findByIp", query = "SELECT c FROM CompAjustesAud c WHERE c.ip = :ip")})



public class CompAjustesAud implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;   
    @Basic(optional = false)
    @Column(name = "ID_AJUSTE")
    private Long idAjuste;   
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "COD_AJUSTE")
    private String codAjuste;
    @Basic(optional = false)
    @Column(name = "ESTADO")
    private Long estado;
    @Column(name = "USUARIO_MODIFICO")
    private String usuarioModifico;
    @Column(name = "OPERACION")
    private String operacion;
    @Column(name = "IP")
    private String ip;

    public CompAjustesAud() {
    }

    public CompAjustesAud(Long id) {
        this.id = id;
    }

    public CompAjustesAud(Long id, Long idAjuste, String descripcion,
            String codAjuste, Long estado) {
        this.id = id;
        this.idAjuste = idAjuste;       
        this.descripcion = descripcion;
        this.codAjuste = codAjuste;
        this.estado = estado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAjuste() {
        return idAjuste;
    }

    public void setIdAjuste(Long idAjuste) {
        this.idAjuste = idAjuste;
    }
   

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodAjuste() {
        return codAjuste;
    }

    public void setCodAjuste(String codAjuste) {
        this.codAjuste = codAjuste;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getUsuarioModifico() {
        return usuarioModifico;
    }

    public void setUsuarioModifico(String usuarioModifico) {
        this.usuarioModifico = usuarioModifico;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CompAjustesAud)) {
            return false;
        }
        CompAjustesAud other = (CompAjustesAud) object;
        if ((this.id == null && other.id != null) || (this.id != null &&
                !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication1.CompAjustesAud[ id=" + id + " ]";
    }
    
}

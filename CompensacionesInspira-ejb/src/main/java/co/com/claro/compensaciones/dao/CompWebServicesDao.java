/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompWebservices;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author asantacb
 */
public class CompWebServicesDao extends AbstractDao<CompWebservices> {

    private Long idws;
    private String url;
    private String descripcion;
    private Long estado;
    private Long idusuario;
    private Date fecha;
    private Long idusuarioMod;   
    private Date fechaMod;

    @PersistenceContext
    private EntityManager entityManager;

    public CompWebServicesDao() {
        super(CompWebservices.class);
    }

    public CompWebServicesDao(EntityManager entityManager) {
        super(CompWebservices.class);
        this.entityManager = entityManager;
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    public Long getIdws() {
        return idws;
    }

    public void setIdws(Long idws) {
        this.idws = idws;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public Long getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Long idusuario) {
        this.idusuario = idusuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getIdusuarioMod() {
        return idusuarioMod;
    }

    public void setIdusuarioMod(Long idusuarioMod) {
        this.idusuarioMod = idusuarioMod;
    }

    /**
     * @return the fechaMod
     */
    public Date getFechaMod() {
        return fechaMod;
    }

    /**
     * @param fechaMod the fechaMod to set
     */
    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

}

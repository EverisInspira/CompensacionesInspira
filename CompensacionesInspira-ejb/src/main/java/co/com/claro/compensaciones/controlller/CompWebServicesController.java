/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.controlller;

import co.com.claro.compensaciones.entity.CompWebservices;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.claro.compensaciones.dao.CompWebServicesDao;
import co.com.claro.compensaciones.util.EJBUtils;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

/**
 *
 * @author asantacb
 */
@Stateless
public class CompWebServicesController {

    @PersistenceContext(unitName = "gestionnew")
    private EntityManager entityManager;

    /**
     * Objetivo: listar datos de tipo CompWebServicesDao
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param descripcion - descripcion que identifica al servicio web
     * @param estado - estado del servicio web
     * @return - Lista de objetos tipo CompWebServicesDao
     * @throws Exception
     */
    public List<CompWebServicesDao> findAll(String descripcion, Long estado)
            throws Exception {
        List<CompWebservices> list = this.getEntityManager()
                .createNamedQuery("CompWebservices.findByDescEst")
                .setParameter("descripcion", descripcion)
                .setParameter("estado", estado)
                .getResultList();
        return convertToDao(list);
    }

    /**
     * Objetivo:metodo realiza la conversion de lista tipo CompWebservices a
     * CompWebServicesDao
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param listEntity - Lista de objetos tipo CompWebservices
     * @retun - lista de objetos tipo CompWebServicesDao
     * @throws java.lang.Exception
     */
    private List<CompWebServicesDao> convertToDao(
            List<CompWebservices> listEntity){
        List<CompWebServicesDao> listDao = new ArrayList<>();   
        try {
            for (CompWebservices compWebservices : listEntity) {
                CompWebServicesDao compWebServicesDao
                        = new CompWebServicesDao();

                    compWebServicesDao = (CompWebServicesDao) EJBUtils.
                            mapperEntityAndDao(compWebServicesDao, 
                                    compWebservices);

                listDao.add(compWebServicesDao);
            }
        } catch (Exception ex) {
                Logger.getLogger(CompWebServicesController.class.getName()).
                        log(Level.SEVERE, null, ex);
        }
        return listDao;      
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}

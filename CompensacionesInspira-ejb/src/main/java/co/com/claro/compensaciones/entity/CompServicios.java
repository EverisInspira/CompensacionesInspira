/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vcastril
 */
@Entity
@Table(name = "COMP_SERVICIOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompServicios.findAll", query = "SELECT c FROM CompServicios c"),
    @NamedQuery(name = "CompServicios.findByIdServicio", query = "SELECT c FROM CompServicios c WHERE c.idServicio = :idServicio"),
    @NamedQuery(name = "CompServicios.findByNombreServicio", query = "SELECT c FROM CompServicios c WHERE c.nombreServicio = :nombreServicio"),
    @NamedQuery(name = "CompServicios.findByCodServicio", query = "SELECT c FROM CompServicios c WHERE c.codServicio = :codServicio"),
    @NamedQuery(name = "CompServicios.findByDescripcion", query = "SELECT c FROM CompServicios c WHERE c.descripcion = :descripcion")})
public class CompServicios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SERVICIO")
    private Float idServicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NOMBRE_SERVICIO")
    private String nombreServicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "COD_SERVICIO")
    private String codServicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public CompServicios() {
    }

    public CompServicios(Float idServicio) {
        this.idServicio = idServicio;
    }

    public CompServicios(Float idServicio, String nombreServicio, String codServicio, String descripcion) {
        this.idServicio = idServicio;
        this.nombreServicio = nombreServicio;
        this.codServicio = codServicio;
        this.descripcion = descripcion;
    }

    public Float getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Float idServicio) {
        this.idServicio = idServicio;
    }

    public String getNombreServicio() {
        return nombreServicio;
    }

    public void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idServicio != null ? idServicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CompServicios)) {
            return false;
        }
        CompServicios other = (CompServicios) object;
        if ((this.idServicio == null && other.idServicio != null) || (this.idServicio != null && !this.idServicio.equals(other.idServicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.ces3.rest.CompServicios[ idServicio=" + idServicio + " ]";
    }
    
}

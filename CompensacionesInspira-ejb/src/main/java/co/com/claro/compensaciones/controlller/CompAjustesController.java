/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.controlller;

import co.com.claro.compensaciones.dao.CompAjustesDao;
import co.com.claro.compensaciones.entity.CompAjustes;
import co.com.claro.compensaciones.util.EJBUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica
 * Descripción: Clase que administra los métodos correspondientes para la
 * fábrica
 *
 * @author Carlos Andrade - everis
 * @version 04/09/2018
 */
@Stateless
public class CompAjustesController {

    @PersistenceContext(unitName = "gestionnew")
    private EntityManager entityManager;

     /**
     * Objetivo:metodo  para la insercion sobre la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustesDao - Objeto que se quiere almacenar     
     */
    public void create(CompAjustesDao compAjustesDao){
        CompAjustes ajustes = convertEntity(compAjustesDao);
        this.getEntityManager().persist(ajustes);
    }
    
    /**
     * Objetivo:metodo  para la actualizacion sobre la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustesDao - Objeto que se quiere actualizar     
     */
    public void edit(CompAjustesDao compAjustesDao) {
        CompAjustes ajustes = convertEntity(compAjustesDao);
        this.getEntityManager().merge(ajustes);
    }

    /**
     * Objetivo:metodo para la eliminación de la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018     
     * @param compAjustesDao     
     */
    public void remove(CompAjustesDao compAjustesDao){
        CompAjustes ajustes = convertEntity(compAjustesDao);
        this.getEntityManager().remove(getEntityManager().
                merge(ajustes));
        
    }
    
     /**
     * Objetivo: listar datos de tipo CompAjustesDao
     *
     * @author vcastril-everis
     * @version 04/09/2018
     * @return - Lista de objetos tipo CompAjustesDao
     * @throws Exception
     */
    public List<CompAjustesDao> findAll()
            throws Exception {
        List<CompAjustes> list = this.getEntityManager()
                .createNamedQuery("CompAjustes.findAll")
                .getResultList();
        return convertToDao(list);
    }
    
    public List<CompAjustesDao>  findDescripcion(String descripcion)
            throws Exception {
         List<CompAjustes> list = this.getEntityManager()
                .createNamedQuery("CompAjustes.findByDescripcion")
                 .setParameter("descripcion", descripcion)
                .getResultList();
        return convertToDao(list);
    }

    public List<CompAjustesDao> findCodigo(String codAjuste) throws Exception {
        List<CompAjustes> list = this.getEntityManager()
                .createNamedQuery("CompAjustes.findByCodAjuste")
                 .setParameter("codAjuste", codAjuste)
                .getResultList();
        return convertToDao(list);
    }

    
    /**
     * Objetivo:metodo realiza la conversion de lista tipo CompAjustes a
     * CompAjustesDao
     *
     * @author candradh-everis
     * @version 05/09/2018
     * @param listEntity - Lista de objetos tipo CompAjustes
     * @retunr - lista de objetos tipo CompAjustesDao
     * @throws java.lang.Exception
     */
    private List<CompAjustesDao> convertToDao(List<CompAjustes> listEntity){
        List<CompAjustesDao> listDao = new ArrayList<>();
        try {
            for (CompAjustes compAjustes : listEntity) {
                CompAjustesDao compAjustesDao
                        = new CompAjustesDao();
                compAjustesDao = (CompAjustesDao) EJBUtils.
                        mapperEntityAndDao(compAjustesDao, compAjustes);
                listDao.add(compAjustesDao);
            }
        } catch (Exception ex) {
            Logger.getLogger(CompWebServicesController.class.getName())
                    .log(Level.SEVERE, null,ex);
        }
        return listDao;
    }
    
    /**
     * Objetivo:metodo realiza la conversion de tipo CompAjustes a
     * CompAjustesDao
     *
     * @author candradh-everis
     * @param compAjustesDao
     * @return
     * @throws Exception 
     */
    private CompAjustes convertEntity(CompAjustesDao compAjustesDao){
        CompAjustes ajustes = new CompAjustes();
        try {
            ajustes = (CompAjustes)
                    EJBUtils.mapperEntityAndDao(ajustes, compAjustesDao);
        } catch (Exception ex) {
            Logger.getLogger(CompAjustesController.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return ajustes;
    }

    /**
     * @return the entityManager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}

package co.com.claro.compensaciones.util;

/**
 * @author asantacb
 */
public class EJBConstants {

    public static final int GET_STATUS_URL_TIMEOUT = 3000;
    public static final String GET_STATUS_URL_HTTP_METHOD = "GET";
    public static final int GET_STATUS_URL_CODE_200 = 200;

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompCausas;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Descripcion: Objeto de transferencia de datos de la tabla COMP_CAUSAS
 *
 * @author vcastril-@everis
 * @version 04/09/2018
 */
public class CompCausasDao extends AbstractDao<CompCausas> 
        implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Long idCausa;   
    private String codCausa;   
    private String descrpcion;   
    private Long estado;  
    private String tipoCausa;
    private String usuario;
    
    @PersistenceContext
    private EntityManager entityManager;

    public CompCausasDao() {
        super(CompCausas.class);
    }

    public CompCausasDao(EntityManager entityManager) {
        super(CompCausas.class);
        this.entityManager = entityManager;
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

   

    /**
     * @return the estado
     */
    public Long getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Long estado) {
        this.estado = estado;
    }

    /**
     * @return the idCausa
     */
    public Long getIdCausa() {
        return idCausa;
    }

    /**
     * @param idCausa the idCausa to set
     */
    public void setIdCausa(Long idCausa) {
        this.idCausa = idCausa;
    }

    /**
     * @return the codCausa
     */
    public String getCodCausa() {
        return codCausa;
    }

    /**
     * @param codCausa the codCausa to set
     */
    public void setCodCausa(String codCausa) {
        this.codCausa = codCausa;
    }

    /**
     * @return the descrpcion
     */
    public String getDescrpcion() {
        return descrpcion;
    }

    /**
     * @param descrpcion the descrpcion to set
     */
    public void setDescrpcion(String descrpcion) {
        this.descrpcion = descrpcion;
    }

    /**
     * @return the tipoCausa
     */
    public String getTipoCausa() {
        return tipoCausa;
    }

    /**
     * @param tipoCausa the tipoCausa to set
     */
    public void setTipoCausa(String tipoCausa) {
        this.tipoCausa = tipoCausa;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
    
    
}

package co.com.claro.compensaciones.facade;

import co.com.claro.compensaciones.controlller.CompCausasController;
import co.com.claro.compensaciones.dao.CompCausasDao;
import co.com.claro.compensaciones.facadelocal.ICompCausasFacadeLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica
 * Descripción: Clase que administra los métodos correspondientes para la
 * fábrica
 *
 * @author Carlos Andrade - everis
 * @version 04/09/2018
 */
@Stateless
public class CompCausasFacade implements ICompCausasFacadeLocal, Serializable{

    private static final long serialVersionUID = 1L;
    
    @EJB
    CompCausasController compCausasController;
    
    /**
     * Objetivo:metodo  para la insercion sobre la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausas - Objeto que se quiere almacenar     
     */
    @Override
    public void create(CompCausasDao compCausas){
        compCausasController.create(compCausas);
    }

    /**
     * Objetivo:metodo  para la actualizacion sobre la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausas - Objeto que se quiere actualizar     
     */
    @Override
    public void edit(CompCausasDao compCausas){
        compCausasController.edit(compCausas);
    }

    /**
     * Objetivo:metodo para la eliminación de la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausas - Objeto que se quiere eliminar     
     */
    @Override
    public void remove(CompCausasDao compCausas) {
        compCausasController.remove(compCausas);
    }

     /**
     * Objetivo:metodo para la eliminacion de un registro sobre
     * la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @return - Lista de objetos tipo CompCausas
     */
    @Override
    public List<CompCausasDao> findAll() {
        List<CompCausasDao> listDao = new ArrayList<>();
        try {
            listDao = compCausasController.findAll();
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return listDao;
    }
    
    @Override
    public List<CompCausasDao> findCodigo(String codCausa){
        List<CompCausasDao> listDao = new ArrayList<>();
        try {
            listDao = compCausasController.findCodigo(codCausa);
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return listDao;
    }

    @Override
    public List<CompCausasDao> findDescripcion(String descripcion){
        List<CompCausasDao> listDao = new ArrayList<>();
        try {
            listDao = compCausasController.findCodigo(descripcion);
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return listDao;
    }
}

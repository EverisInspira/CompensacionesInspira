package co.com.claro.compensaciones.facadelocal;

import co.com.claro.compensaciones.dao.CompCausasDao;
import java.util.List;
import javax.ejb.Local;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica 
 * Descripción: Clase que administra los métodos correspondientes para la 
 * fábrica 
 * @author Carlos Andrade  - everis
 * @version 04/09/2018  
 */
@Local
public interface ICompCausasFacadeLocal {
        
     /**
     * Objetivo:metodo  para la insercion sobre la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausas - Objeto que se quiere almacenar  
     */
    void create(CompCausasDao compCausas);
       
    /**
     * Objetivo:metodo  para la actualizacion sobre la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausas - Objeto que se quiere actualizar    
     */
    void edit(CompCausasDao compCausas);
        
    /**
     * Objetivo:metodo para la eliminación de la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compCausas - Objeto que se quiere eliminar     
     */
    void remove(CompCausasDao compCausas);
    
     /**
     * Objetivo:metodo para la eliminacion de un registro sobre
     * la entidad CompCausas
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @return - Lista de objetos tipo CompCausas
     */
    public List<CompCausasDao> findAll();

    public List<CompCausasDao> findCodigo(String codCausa);

    public List<CompCausasDao> findDescripcion(String descripcion);
         
}

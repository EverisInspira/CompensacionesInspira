package co.com.claro.compensaciones.facade;

import co.com.claro.compensaciones.controlller.CompAjustesController;
import co.com.claro.compensaciones.dao.CompAjustesDao;
import co.com.claro.compensaciones.facadelocal.ICompAjustesFacadeLocal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica
 * Descripción: Clase que administra los métodos correspondientes para la
 * fábrica
 *
 * @author Carlos Andrade - everis
 * @version 04/09/2018
 */
@Stateless
public class CompAjustesFacade implements ICompAjustesFacadeLocal, Serializable{

    private static final long serialVersionUID = 1L;
    
    @EJB
    CompAjustesController compAjustesController;
    
    /**
     * Objetivo:metodo  para la insercion sobre la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustes - Objeto que se quiere almacenar  
     */
    @Override
    public void create(CompAjustesDao compAjustes){
        compAjustesController.create(compAjustes);
    }
    
    /**
     * Objetivo:metodo  para la actualizacion sobre la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustes - Objeto que se quiere actualizar    
     */
    @Override
    public void edit(CompAjustesDao compAjustes){
        compAjustesController.edit(compAjustes);
    }

    /**
     * Objetivo:metodo para la eliminación de la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018     
     * @param ajustesDao      
     */
    @Override
    public void remove(CompAjustesDao ajustesDao){
        compAjustesController.remove(ajustesDao);
    }

    /**
     * Objetivo:metodo para la eliminacion de un registro sobre
     * la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @return - Lista de objetos tipo CompAjustes
     */
    @Override
    public List<CompAjustesDao> findAll() {
        List<CompAjustesDao> ajustesDaos = new ArrayList<>();
        try {
            ajustesDaos = compAjustesController.findAll();
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
            
        }
        return ajustesDaos;
    }
    
    @Override
    public List<CompAjustesDao> findCodigo(String codAjuste){
        List<CompAjustesDao> ajustesDaos = new ArrayList<>();
        try {
            ajustesDaos= compAjustesController.findCodigo(codAjuste);
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
            
        }
        return ajustesDaos;
    }
    
    @Override
    public List<CompAjustesDao> findDescripcion(String descripcion){
        List<CompAjustesDao> ajustesDaos = new ArrayList<>();
        try {
            ajustesDaos= compAjustesController.findDescripcion(descripcion);
        } catch (Exception ex) {
            Logger.getLogger(CompWebservicesFacade.class.getName()).
                    log(Level.SEVERE, null, ex);
            
        }
        return ajustesDaos;
    }
}

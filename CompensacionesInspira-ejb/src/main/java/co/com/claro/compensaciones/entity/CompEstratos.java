/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vcastril
 */
@Entity
@Table(name = "COMP_ESTRATOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompEstratos.findAll", query = "SELECT c FROM CompEstratos c"),
    @NamedQuery(name = "CompEstratos.findByIdEstrato", query = "SELECT c FROM CompEstratos c WHERE c.idEstrato = :idEstrato"),
    @NamedQuery(name = "CompEstratos.findByEstrato", query = "SELECT c FROM CompEstratos c WHERE c.estrato = :estrato"),
    @NamedQuery(name = "CompEstratos.findByDescripcion", query = "SELECT c FROM CompEstratos c WHERE c.descripcion = :descripcion")})
public class CompEstratos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESTRATO")
    private Float idEstrato;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ESTRATO")
    private String estrato;
    @Size(max = 255)
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public CompEstratos() {
    }

    public CompEstratos(Float idEstrato) {
        this.idEstrato = idEstrato;
    }

    public CompEstratos(Float idEstrato, String estrato) {
        this.idEstrato = idEstrato;
        this.estrato = estrato;
    }

    public Float getIdEstrato() {
        return idEstrato;
    }

    public void setIdEstrato(Float idEstrato) {
        this.idEstrato = idEstrato;
    }

    public String getEstrato() {
        return estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstrato != null ? idEstrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CompEstratos)) {
            return false;
        }
        CompEstratos other = (CompEstratos) object;
        if ((this.idEstrato == null && other.idEstrato != null) || (this.idEstrato != null && !this.idEstrato.equals(other.idEstrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.ces3.rest.CompEstratos[ idEstrato=" + idEstrato + " ]";
    }
    
}

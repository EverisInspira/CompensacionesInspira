package co.com.claro.compensaciones.facadelocal;

import co.com.claro.compensaciones.dao.CompAjustesDao;
import java.util.List;
import javax.ejb.Local;

/**
 * Objetivo: Administrar los métodos correspondientes para la fábrica 
 * Descripción: Clase que administra los métodos correspondientes para la 
 * fábrica 
 * @author Carlos Andrade  - everis
 * @version 04/09/2018  
 */
@Local
public interface ICompAjustesFacadeLocal{
        
     /**
     * Objetivo:metodo  para la insercion sobre la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustes - Objeto que se quiere almacenar   
     */
    void create(CompAjustesDao compAjustes);
       
    /**
     * Objetivo:metodo  para la actualizacion sobre la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @param compAjustes - Objeto que se quiere actualizar   
     */
    void edit(CompAjustesDao compAjustes);
        
    /**
     * Objetivo:metodo para la eliminación de la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018     
     * @param ajustesDao   
     */
    void remove(CompAjustesDao ajustesDao);
    
     /**
     * Objetivo:metodo para la eliminacion de un registro sobre
     * la entidad CompAjustes
     *
     * @author candradh-everis
     * @version 04/09/2018
     * @return - Lista de objetos tipo CompAjustes
     */
    public List<CompAjustesDao> findAll();

    /**
     * 
     * @param codAjuste
     * @return 
     */
    public List<CompAjustesDao> findCodigo(String codAjuste);

    /**
     * 
     * @param codAjuste
     * @return 
     */
    public List<CompAjustesDao> findDescripcion(String codAjuste);
         
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Descripcion: mapea la tabla de base de datos de COMP_CAUSAS
 *
 * @author vcastril-@everis
 * @version 04/09/2018
 */
@Entity
@Table(name = "COMP_CAUSAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompCausas.findAll",
            query = "SELECT c FROM CompCausas c"),
    @NamedQuery(name = "CompCausas.findByIdCausa", 
            query = "SELECT c FROM CompCausas c WHERE c.idCausa = :idCausa"),
    @NamedQuery(name = "CompCausas.findByCodCausa",
            query = "SELECT c FROM CompCausas c WHERE c.codCausa = :codCausa"),
    @NamedQuery(name = "CompCausas.findByDescrpcion",
            query = "SELECT c FROM CompCausas c "
                    + "WHERE c.descrpcion = :descrpcion"),
    @NamedQuery(name = "CompCausas.findByEstado", 
            query = "SELECT c FROM CompCausas c WHERE c.estado = :estado"),
    @NamedQuery(name = "CompCausas.findByTipoCausa", 
            query = "SELECT c FROM CompCausas c "
                    + "WHERE c.tipoCausa = :tipoCausa")})
@SequenceGenerator(name="idCausa", sequenceName="seq_COMP_CAUSAS",
            initialValue=1, allocationSize=1) 
public class CompCausas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CAUSA")
    @GeneratedValue(generator = "idCausa", 
            strategy = GenerationType.SEQUENCE)
    private Long idCausa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "COD_CAUSA")
    private String codCausa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "DESCRPCION")
    private String descrpcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Long estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "TIPO_CAUSA")
    private String tipoCausa;
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    @Column(name = "USUARIO")
    private String usuario;

    public CompCausas() {
    }

    public CompCausas(Long idCausa) {
        this.idCausa = idCausa;
    }

    public CompCausas(Long idCausa, String codCausa, String descrpcion,
            Long estado, String tipoCausa) {
        this.idCausa = idCausa;
        this.codCausa = codCausa;
        this.descrpcion = descrpcion;
        this.estado = estado;
        this.tipoCausa = tipoCausa;
    }

    public Long getIdCausa() {
        return idCausa;
    }

    public void setIdCausa(Long idCausa) {
        this.idCausa = idCausa;
    }

    public String getCodCausa() {
        return codCausa;
    }

    public void setCodCausa(String codCausa) {
        this.codCausa = codCausa;
    }

    public String getDescrpcion() {
        return descrpcion;
    }

    public void setDescrpcion(String descrpcion) {
        this.descrpcion = descrpcion;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    public String getTipoCausa() {
        return tipoCausa;
    }

    public void setTipoCausa(String tipoCausa) {
        this.tipoCausa = tipoCausa;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
 
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCausa != null ? idCausa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CompCausas)) {
            return false;
        }
        CompCausas other = (CompCausas) object;
        if ((this.idCausa == null && other.idCausa != null) || 
                (this.idCausa != null && !this.idCausa.equals(other.idCausa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.ces3.rest.CompCausas[ idCausa=" + idCausa + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompEstratos;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author candradh
 */
public class CompEstratosDao extends AbstractDao<CompEstratos> {
    
     private Float idEstrato;   
    private String estrato;   
    private String descripcion;
    
    
    @PersistenceContext
    private EntityManager entityManager;

    public CompEstratosDao() {
        super(CompEstratos.class);
    }

    public CompEstratosDao(EntityManager entityManager) {
        super(CompEstratos.class);
        this.entityManager = entityManager;
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @return the idEstrato
     */
    public Float getIdEstrato() {
        return idEstrato;
    }

    /**
     * @param idEstrato the idEstrato to set
     */
    public void setIdEstrato(Float idEstrato) {
        this.idEstrato = idEstrato;
    }

    /**
     * @return the estrato
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * @param estrato the estrato to set
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

   
    
    
}

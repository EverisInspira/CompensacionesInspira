/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.dao;

import co.com.claro.compensaciones.entity.CompAjustes;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Descripcion: Objeto de transferencia de datos de la tabla Comp_Causas_Aud
 *
 * @author candradh-@everis
 * @version 10/09/2018
 */
public class CompCausasAudDao extends AbstractDao<CompAjustes> {
    
    private Long id;    
    private Long idCausa;   
    private String descripcion;   
    private String codCausa;    
    private Long estado;    
    private String usuarioModifico;    
    private String operacion;    
    private String ip;
    
    
    @PersistenceContext
    private EntityManager entityManager;

    public CompCausasAudDao() {
        super(CompAjustes.class);
    }

    public CompCausasAudDao(EntityManager entityManager) {
        super(CompAjustes.class);
        this.entityManager = entityManager;
    }


    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @return the idCausa
     */
    public Long getIdAjuste() {
        return idCausa;
    }

    public void setIdAjuste(Long idCausa) {
        this.idCausa = idCausa;
    }

    public String getCodAjuste() {
        return codCausa;
    }

    public void setCodAjuste(String codCausa) {
        this.codCausa = codCausa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }    

    /**
     * @return the usuarioModifico
     */
    public String getUsuarioModifico() {
        return usuarioModifico;
    }

    /**
     * @param usuarioModifico the usuarioModifico to set
     */
    public void setUsuarioModifico(String usuarioModifico) {
        this.usuarioModifico = usuarioModifico;
    }

    /**
     * @return the operacion
     */
    public String getOperacion() {
        return operacion;
    }

    /**
     * @param operacion the operacion to set
     */
    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
    
    
}

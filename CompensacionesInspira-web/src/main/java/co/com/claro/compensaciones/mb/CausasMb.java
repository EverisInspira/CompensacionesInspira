/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.mb;

import co.com.claro.compensaciones.dao.CompCausasDao;
import co.com.claro.compensaciones.facadelocal.ICompCausasFacadeLocal;
import co.com.claro.compensaciones.util.Constants;
import co.com.claro.compensaciones.util.Utils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * Descripcion: clase que realiza funcionalidades de paametrización sobre 
 * las causas de compensaciones 
 *
 * @author vcastril-@everis
 * @version 04/09/2018
 */
@ManagedBean(name = "causasMb")
@SessionScoped
public class CausasMb implements Serializable{
   
    private static final long serialVersionUID = 1L;
    /**
     * Codigo causa
     */
    private String codCausa;
    /**
     * Codigo causa
     */
    private String usuario;
    /**
     * Codigo causa
     */
    private String usuarioLogueado;
    /**
     * Causa
     */
    private CompCausasDao causa;
    /**
     * Descipción
     */
    private String descripcion;
    /**
     * Estado
     */
    private String estado;
    /**
     * tipo Causa
     */
    private String tipoCausa;
    /**
     * Información 
     */
    private String info;
    /**
     * Lista Tabla
     */
    private List<CompCausasDao> listTabla;
    /**
     * Compensación Causas
     */
    private CompCausasDao compCausasDao= null;

    @EJB
    public ICompCausasFacadeLocal compCausasFacadeLocal;

    /**
     * Constructor de la clase 
     */
    @PostConstruct
    public void init() {  
        try {
            setListTabla(compCausasFacadeLocal.findAll());
            InicioSesionMb inicioSesionMb
                = Utils.getBean("inicioSesionMb");
            usuarioLogueado = inicioSesionMb.getUsuario();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CausasMb.class.getName())
                    .log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Descripcion: limpia los campos de la vista causas  
     *
     * @author vcastril-@everis
     */
    public void nuevo(){
        codCausa = null;
        descripcion = null;
        estado = null;
    }
    /**
     * Descripcion: actualiza los campos de la vista causas  
     *
     * @author vcastril-@everis
     * @throws Exception 
     */
    public void actualizar() throws Exception{
        boolean validacion = validarCampos();
        if(validacion){
            compCausasDao.setCodCausa(codCausa);
            compCausasDao.setDescrpcion(descripcion);
            compCausasDao.setEstado(Long.parseLong(estado));
            compCausasDao.setTipoCausa(tipoCausa);
            compCausasDao.setUsuario(usuarioLogueado);
            compCausasFacadeLocal.edit(compCausasDao);
        }
        nuevo();
        setListTabla(compCausasFacadeLocal.findAll());
    }
    /**
     * Descripcion: guarda los campos de la vista causas  
     *
     * @author vcastril-@everis
     * @throws Exception 
     */
    public void guardar() throws Exception{
        boolean validacion = validarCampos();
        boolean validacionCodigoDescripcion = validarCodigo();
        compCausasDao = new CompCausasDao();
        if(validacion && validacionCodigoDescripcion){
            compCausasDao.setCodCausa(codCausa);
            compCausasDao.setDescrpcion(descripcion);
            compCausasDao.setEstado(Long.parseLong(estado));
            compCausasDao.setTipoCausa(tipoCausa);
            compCausasDao.setUsuario(usuarioLogueado);
            compCausasFacadeLocal.create(compCausasDao);
        }           
        nuevo();
        setListTabla(compCausasFacadeLocal.findAll());
    }
    
    /**
     * Descripcion: Eliminar los campos de la vista causas  
     *
     * @author vcastril-@everis
     * @throws Exception 
     */
    public void delete() throws Exception{
        if(causa != null && !causa.equals("")){
            compCausasFacadeLocal.remove(causa);
        }
        setListTabla(compCausasFacadeLocal.findAll());
    }
    
    /**
     * Descripcion: Seleccionar los campos de la vista causas  
     *
     * @author vcastril-@everis
     * @param causasDao 
     */
    public void preEditar(CompCausasDao causasDao){
        codCausa = causasDao.getCodCausa();
        descripcion = causasDao.getDescrpcion();
        estado = String.valueOf(causasDao.getEstado());
        tipoCausa = causasDao.getTipoCausa();
        compCausasDao = causasDao;
        
    }
    
    public boolean validarCodigo(){
        List<CompCausasDao> compCausasCodigo  = compCausasFacadeLocal.
                findCodigo(codCausa);
        List<CompCausasDao> compCausasDescripcion  = compCausasFacadeLocal.
                findDescripcion(descripcion);
        return compCausasCodigo.isEmpty() && compCausasDescripcion.isEmpty();
    }
    
    /**
     * Descripcion: Validar los campos de la vista causas  
     *
     * @author vcastril-@everis
     * @return 
     */
    public boolean validarCampos(){
        if(codCausa!= null && !codCausa.equals("")){
            if(descripcion!= null && !descripcion.equals("")){
                if(estado!= null && !estado.equals("")){
                    if(tipoCausa!= null && !tipoCausa.equals("")){
                        return true;
                    }else{
                        info = Constants.INGRESE_ESTADO;
                    }   
                }else{
                    info = Constants.INGRESE_ESTADO;
                }  
            }else{
                info = Constants.INGRESE_DESCRIPCION;
            }
        }else{
            info = Constants.INGRESE_CODIGO;
        }
        return false;
    }

    /**
     * 
     * @return codCausa
     */
    public String getCodCausa() {
        return codCausa;
    }

    /**
     * 
     * @param codCausa 
     */
    public void setCodCausa(String codCausa) {
        this.codCausa = codCausa;
    }

    /**
     * 
     * @return causa
     */
    public CompCausasDao getCausa() {
        return causa;
    }

    /**
     * 
     * @param causa 
     */
    public void setCausa(CompCausasDao causa) {
        this.causa = causa;
    }

    /**
     * 
     * @return descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * 
     * @param descripcion 
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * 
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * 
     * @param estado 
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * 
     * @return 
     */
    public String getTipoCausa() {
        return tipoCausa;
    }

    /**
     * 
     * @param tipoCausa 
     */
    public void setTipoCausa(String tipoCausa) {
        this.tipoCausa = tipoCausa;
    }

    /**
     * 
     * @return info
     */
    public String getInfo() {
        return info;
    }

    /**
     * 
     * @param info 
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * 
     * @return listTabla
     */
    public List<CompCausasDao> getListTabla() {
        return listTabla;
    }

    /**
     * 
     * @param listTabla 
     */
    public void setListTabla(List<CompCausasDao> listTabla) {
        this.listTabla = listTabla;
    }

    /**
     * 
     * @return compCausasDao
     */
    public CompCausasDao getCompCausasDao() {
        return compCausasDao;
    }

    /**
     * 
     * @param compCausasDao 
     */
    public void setCompCausasDao(CompCausasDao compCausasDao) {
        this.compCausasDao = compCausasDao;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuarioLogueado() {
        return usuarioLogueado;
    }

    public void setUsuarioLogueado(String usuarioLogueado) {
        this.usuarioLogueado = usuarioLogueado;
    }
    
    
    
}

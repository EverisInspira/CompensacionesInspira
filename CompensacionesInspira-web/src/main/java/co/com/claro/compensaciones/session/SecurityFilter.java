package co.com.claro.compensaciones.session;


import co.com.claro.compensaciones.mb.InicioSesionMb;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Objetivo: controlar la sesion 
 * @Descripción: clase que permite contener métodos que controlan lo del inicio
 * de sesión 
 * 
 * @author Valentina Castrillon- everis
 * @Versión: 1.0
 */
public class SecurityFilter implements Filter, Serializable {

    private static final boolean DEBUG = true;

    private FilterConfig filterConfig = null;

    /**
     * 
     * @param filterConfig
     * @throws ServletException 
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        if (filterConfig != null && DEBUG) {
            log("SecurityFilter:Initializing filter");

        }
    }
    
    /**
     * Objetivo: controlar sesión 
     * Descripción: validar los datos y 
     * redireccionar la la página coespondiente 
     * 
     * @author Valentina Castrillon- everis 
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException 
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        if (DEBUG) {
            log("SecurityFilter:doFilter()");
        }

        doBeforeProcessing(request, response);
        HttpServletRequest req = (HttpServletRequest) request;

        HttpServletResponse resp = (HttpServletResponse) response;

        String url = req.getServletPath();

        InicioSesionMb autenticacionBean
                = (InicioSesionMb) req.getSession()
                .getAttribute("inicioSesionMb");
        if (autenticacionBean != null) {
            if ((autenticacionBean.getUsuario() != null
                    && !autenticacionBean.getUsuario().isEmpty())
                    && !autenticacionBean.getAutenticado()
                    && autenticacionBean.getClave() != null
                    && !autenticacionBean.getClave().isEmpty()) {
                try {
                    autenticacionBean.autenticar();
                } catch (Exception ex) {
                    Logger.getLogger(SecurityFilter.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
            } else if ((autenticacionBean.getUsuario() == null
                    || autenticacionBean.getUsuario().isEmpty())&&
                    !autenticacionBean.getAutenticado()) {
                resp.sendRedirect("/CompensacionesInspira-web/"
                        .concat("login.xhtml"));
            }
        } else {
            resp.sendRedirect("/CompensacionesInspira-web/"
                        .concat("login.xhtml"));
        }
        Throwable problem = null;
        try {
            chain.doFilter(request, response);
        } catch (IOException | ServletException t) {
            Logger.getLogger(SecurityFilter.class.getName())
                            .log(Level.SEVERE, null, t);
        }

        doAfterProcessing(request, response);
        if (problem != null) {
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }
            sendProcessingError(problem, response);
        }
        }

    /**
     * 
     */
    @Override
    public void destroy() {

    }

    /**
     * Objetivo: Hacer antes del procesamiento
     * Descripción: Hacer antes del procesamiento
     * 
     * @author Valentina Castrillon- everis 
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException 
     */
    private void doBeforeProcessing(ServletRequest request,
            ServletResponse response)
            throws IOException, ServletException {
        if (DEBUG) {
            log("SecurityFilter:DoBeforeProcessing");
        }

    }

    /**
     * Objetivo: Hacer despues del procesamiento
     * Descripción: Hacer despues del procesamiento
     * 
     * @author Valentina Castrillon- everis 
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException 
     */
    private void doAfterProcessing(ServletRequest request,
            ServletResponse response)
            throws IOException, ServletException {
        if (DEBUG) {
            log("SecurityFilter:DoAfterProcessing");
        }
    }

    /**
     * 
     * @param msg 
     */
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

    /**
     * Objetivo: enviar error 
     * Descripción: metodo que envia error de procesamiento
     * 
     * @author Valentina Castrillon- everis 
     * @param t
     * @param response 
     */
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);

        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n"
                        .concat("<body>\n"));
                pw.print("<h1>The resource did not process correctly</h1>\n"
                        .concat("<pre>\n"));
                pw.print(stackTrace);
                pw.print("</pre></body>\n</html>");
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
                Logger.getLogger(SecurityFilter.class.getName())
                            .log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
                 Logger.getLogger(SecurityFilter.class.getName())
                            .log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Objetivo: obtener rastreo de pila 
     * Descripción: metodo que obtiene rastreo de pila 
     * 
     * @author Valentina Castrillon- everis
     * @param t
     * @return 
     */
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    /**
     * Return the filter configuration object for this filter.
     * @return 
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("SecurityFilter()");
        }
        StringBuffer sb = new StringBuffer("SecurityFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
}

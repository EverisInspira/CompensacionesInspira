/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.util;

/**
 *
 * @author halmarza
 */
public class Constants {

    public Constants() {
    }

    
    
    public static final String  WS_REST_MSJ_ERROR_USER = "Usuario incorrecto.";
    public static final String  WS_REST_MSJ_ERROR_PASS = "Contraseña incorrecta.";
    public static final String  WS_REST_MSJ_ERROR_USER_PASS = "Usuario y contraseña incorrectos.";
    
    public static final String  MSJ_ERROR_USER = "Favor diligenciar el usuario.";
    public static final String  MSJ_ERROR_PASS = "Favor diligenciar la contraseña.";
    public static final String  MSJ_ERROR_USER_PASS = "Favor diligenciar el usuario y la contraseña.";
    
    public static final String  EXPRESION_REGULAR_VALIDAR__USER = "[a-zA-Z].*";
    
    
     public static final String  MSJ_ERROR_CARACTERES_ESPECIALES_USER = "El usuario no debe contener caracteres especiales.";
    
    public static final String WS_REST_FIJA_MSJ_ERROR_DISPONIBILIDAD
            = "No es posible conectarse con el servicio de autenticación";
    
    public static final String WS_REST_MSJ_ERROR_BD = "No se encuentra el Servicio ldap fija";
    
    public static final int GET_STATUS_URL_TIMEOUT = 3000;
    public static final String GET_STATUS_URL_HTTP_METHOD = "GET";
    public static final int GET_STATUS_URL_CODE_200 = 200;
    public static final Character WS_REST_ESTADO_ACTIVO = '1';
    public static final Character WS_REST_ESTADO_INACTIVO = '0';
    public static final String SERVICIO_LDAP_FIJA = "SERVICIO_LDAP_FIJA";
    public static final String WS_REST_PTLU_FIJA_REM_WSDL = "?wsdl";
    public static final String WS_FIJA_RESP_ERROR = "0";
    public static final String WS_REST_FIJA_MSJ_ERROR_RESPO
            = "ERROR AL AUTENTICAR,VERIFIQUE SUS CREDENCIALES";

    public static final String WS_REST_PTLU_SSL_SALTAR = "SSL";
    public static final String WS_REST_PTLU_SOAP_FIJA_POST = "POST";
    public static final String WS_REST_PTLU_SOAP_FIJA_CONTENT_TYPE
            = "Content-type";
    public static final String WS_REST_PTLU_SOAP_FIJA_TYPE_MIME
            = "text/xml; charset=utf-8";
    public static final String WS_REST_PTLU_SOAP_FIJA_ACTION_PROPTR
            = "SOAPAction";
    public static final String WS_REST_PTLU_SOAP_FIJA_ACTION_METODO
 = "https://portalusuarios.telmexla.com.co/WbService/2Wbintranet.php/Autentica";

    public static final String WS_REQUEST_XML_FIJA = "<soapenv:Envelope\n"
    + "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
    + "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n"
    + "    xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\"\n"
    + "    xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\"\n"
    + "    xmlns:wbs=\"WbService\"\n"
    + ">\n"
    + "    <soapenv:Header/>\n"
    + "    <soapenv:Body>\n"
    + "        <wbs:Autentica soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n"
    + "            <usuario xsi:type=\"xsd:string\">USER</usuario>\n"
    + "            <password xsi:type=\"xsd:string\">PASS</password>\n"
    + "            <hash xsi:type=\"xsd:string\">HASH1</hash>\n"
    + "        </wbs:Autentica>\n"
    + "    </soapenv:Body>\n"
    + "</soapenv:Envelope>\n"
    + "";

    public static final String WS_REST_PTLU_SOAP_FIJA_REQ_USER = "USER";
    public static final String WS_REST_PTLU_SOAP_FIJA_REQ_PASS = "PASS";
    public static final String WS_REST_PTLU_SOAP_FIJA_REQ_HASH = "HASH1";

    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_F1
            = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:tns=\"urn:WbService\">";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_R1
            = "<soapenv:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  xmlns:wbs=\"WbService\" xmlns:soapenv=\"http://www.w3.org/2003/05/soap-envelope\">";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_F2
            = "<ns1:AutenticaResponse xmlns:ns1=\"\">";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_R2
            = "<wbs:Autentica soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_F3
            = "</ns1:AutenticaResponse>";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_R3
            = "</wbs:Autentica>";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_F4
            = "SOAP-ENV:";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_R4
            = "soapenv:";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_F5
            = "ns1:AutenticaResponse";
    public static final String WS_REST_PTLU_SOAP_FIJA_RES_REP_R5
            = "wbs:Autentica";

    public static final String WS_REST_PTLU_SOAP_FIJA_RES_NOMBRE = "NOMBRE";

    public static final String WS_REST_PTLU_SOAP_FIJA_RES_CEDULA = "CEDULA";
    public static final String INGRESE_CODIGO = "Ingrese Código";
    public static final String INGRESE_TIPO_AJUSTE = "Ingrese Tipo Ajuste";
    public static final String INGRESE_DESCRIPCION = "Ingrese Descipción";
    public static final String INGRESE_ESTADO = "Ingrese Estado";
    
    
    /**
     * Enums de menu
     */
    public static enum ITEM_MENU {
        INICIO("Inicio"),
        CARGUE("Cargue"),
        SINTOMA("Sintomas"),
        ESTIMACION("Estados"),
        AJUSTE("Ajuste"),
        CAUSA("Causa"),
        SAP("Sap"),
        COM("COM"),
        RES("RES"),
        NOVEDAD("Novedades"),
        NODOSUNIDAD("NodosUnidad"),
        CAUSAAVERIANODO("CausaAveriaNodo"),
        DESCAVERIANODO("DescAveriaNodo"),
        DESCPROBLNODO("DescProblNodo"),
        CUENTAS("Cuentas"),
        REPORTECUENTAS("ReporteCuentas"),
        REPORTENODOS("ReporteNodos"),
        SINTOMANUEVOSAP("SintomaNuevoSap"),
        NODOS("Nodos"),
        OBSERVACIONES("ObservacionRechazo"),
        AJUSTERESIDENCIATIPO("Ajuste Residencial Tipo"),
        AJUSTERESIDENCIACOD("Ajuste Residencial Codigo"),
        DISCONTINUAS("Discontinuas"),
        PRUEBA("Prueba"),
        CERRARSESION("Cerrar"),
        APROBADOS("Aprobados"),
        RECHAZADOS("Rechazados"),
        PRUE("Prueba");
        String descripcion;

        private ITEM_MENU(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getDescription() {
            return descripcion;
        }
    }
    
}

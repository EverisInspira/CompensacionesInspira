package co.com.claro.compensaciones.ws.cliente.login;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value = XmlAccessType.FIELD)
@XmlType(name = "wbs:Autentica", propOrder = {"usuario", "password"})
public class RequestFija {

    @XmlElement(name = "usuario", required = true)
    private String usuario;
    @XmlElement(name = "password", required = true)
    private String password;

    public RequestFija() {
        // <editor-fold defaultstate="collapsed" desc="Compiled Code">
        /* 0: aload_0
         * 1: invokespecial java/lang/Object."<init>":()V
         * 4: return
         *  */
        // </editor-fold>
    }

    public RequestFija(String usuario, String password) {
        
        this.usuario = usuario;
        this.password = password;
        
        // <editor-fold defaultstate="collapsed" desc="Compiled Code">
        /* 0: aload_0
         * 1: invokespecial java/lang/Object."<init>":()V
         * 4: aload_0
         * 5: aload_1
         * 6: putfield      co/com/claro/portalusuarios/ejb/compartido/RequestFija.usuario:Ljava/lang/String;
         * 9: aload_0
         * 10: aload_2
         * 11: putfield      co/com/claro/portalusuarios/ejb/compartido/RequestFija.password:Ljava/lang/String;
         * 14: return
         *  */
        // </editor-fold>
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

   
}

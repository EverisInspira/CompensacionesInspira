/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.mb;

import co.com.claro.compensaciones.util.Constants;
import co.com.claro.compensaciones.util.Utils;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author candradh
 */
@ManagedBean(name = "menuInicioMb")
@ViewScoped
public class MenuInicioMb {
    
     private static final long serialVersionUID = 1L;
     
     /**
     * Objetivo: ingresa a cada una de las opciones del menu Descripción: Método
     * que permite redieccionar a cada una de las opciones del menu
     *
     * @author candradh-everis
     * @param caseMenu
     */
    public void irMenu(String caseMenu){
        Constants.ITEM_MENU seleccionado
                = Constants.ITEM_MENU.valueOf(caseMenu);
        switch (seleccionado) {
            case INICIO:
                Utils.redirectPage("/secure/menu/menuPrincipal.xhtml");
                break;
            case AJUSTE:
                Utils.redirectPage("/secure/parametrizacion/ajustes/configuracionAjusteOrigen.xhtml");
                break;
            case CAUSA:
                Utils.redirectPage("/secure/parametrizacion/causas/configuracionCausasOrigen.xhtml");
                break;           
            case CERRARSESION:
                Utils.logout();
                break;
            default:
                break;
        }
    }
     
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.util;

import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author amendega
 */
public final class MessageUtil {

    private static final String ID_COMPONENT_FORM = "formModal";
    private static final String ID_COMPONENT_MESSAGE = "messageGeneric"; 

    /** 
     * @param facesMessage
     */
    public static void addMessage(FacesMessage facesMessage) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(ID_COMPONENT_MESSAGE, facesMessage); 
    }

    /**
     * @description representa los niveles de severidad de los mensajes.
     * @param message
     * @param severity
     */
    private static void addMessageWithSeverity(String message, 
            Severity severity) {

        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage facesMsg
                = new FacesMessage(severity, message, null); 
        context.addMessage(ID_COMPONENT_MESSAGE, facesMsg); 
        context.getPartialViewContext().getRenderIds().add(ID_COMPONENT_FORM); 
    }

    /**
     * @param message
     * @description representa el mensaje de advertencia
     */
    public static void addMessageWarning(String message) {
        addMessageWithSeverity(message, FacesMessage.SEVERITY_WARN);
    }

    /**
     * @description representa el mensaje de informativo
     * @param message
     */
    public static void addMessageInfo(String message) {
        addMessageWithSeverity(message, FacesMessage.SEVERITY_INFO);
    }

    /**
     * @description representa el mensaje de error
     * @param message
     */
    public static void addMessageError(String message) {
        addMessageWithSeverity(message, FacesMessage.SEVERITY_ERROR);
    }

    /**
     * @param message
     * @description representa el mensaje de errores fatales
     */
    public static void addMessageFatal(String message) {
        addMessageWithSeverity(message, FacesMessage.SEVERITY_FATAL);
    }

    /**
     * @description verifica si tiene mensajes
     * @return
     */
    public static boolean hasMessages() {
        Iterator<FacesMessage> iterable
                = FacesContext.getCurrentInstance().getMessages();
        while (iterable.hasNext()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * @description valida si el mensaje tiene errores
     * @return
     */
    public static boolean hasWarnOrHigherMessage() {
        boolean hasError = Boolean.FALSE;
        FacesContext context = FacesContext.getCurrentInstance();
        List<FacesMessage> msgList = context.getMessageList();
        for (FacesMessage fm : msgList) {
            if (fm.getSeverity() == FacesMessage.SEVERITY_WARN
                    || fm.getSeverity() == FacesMessage.SEVERITY_ERROR
                    || fm.getSeverity() == FacesMessage.SEVERITY_FATAL) {
                hasError = Boolean.TRUE;
                break;
            }
        }

        return hasError;
    }

    /**
     * @description limpia los mensajes del contexto
     * @param severity
     */
    public static void clearMessages(Severity severity) {
        Iterator<FacesMessage> iterable
                = FacesContext.getCurrentInstance().getMessages();
        while (iterable.hasNext()) {
            FacesMessage fm = iterable.next();
            if (severity == null || severity.getOrdinal()
                    == fm.getSeverity().getOrdinal()) {
                iterable.remove();
            }
        }
    }

    public static void throwNew(FacesMessage message) {
        throw new ValidatorException(message);

    }

}

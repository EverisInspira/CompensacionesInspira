/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.util;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author amendega
 */
@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController implements Serializable {
    
   
     @ManagedProperty(value = "#{param.url}")
     private String url; 
     
     
     public void showPage(){ 
         Utils.redirectPage(getUrl());  
     }
     /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the page to set
     */
    public void setUrl(String url) {
        this.url = url; 
    }
     

    public void reload() throws IOException{
        Utils.reload();
    }
        
}

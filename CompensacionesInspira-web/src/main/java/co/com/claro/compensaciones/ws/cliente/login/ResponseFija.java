package co.com.claro.compensaciones.ws.cliente.login;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(value = XmlAccessType.FIELD)
@XmlType(name = "ns1:AutenticaResponse", propOrder = {"nombre", "cedula"})
public class ResponseFija {

    @XmlElement(name = "nombre", required = true)
    private String nombre;
    @XmlElement(name = "cedula", required = true)
    private String cedula;

    public ResponseFija() {
        // <editor-fold defaultstate="collapsed" desc="Compiled Code">
        /* 0: aload_0
         * 1: invokespecial java/lang/Object."<init>":()V
         * 4: return
         *  */
        // </editor-fold>
    }

    public ResponseFija(String nombre, String cedula) {
        
        this.nombre = nombre;
        this.cedula = cedula;
        
        // <editor-fold defaultstate="collapsed" desc="Compiled Code">
        /* 0: aload_0
         * 1: invokespecial java/lang/Object."<init>":()V
         * 4: aload_0
         * 5: aload_1
         * 6: putfield      co/com/claro/portalusuarios/ejb/compartido/ResponseFija.nombre:Ljava/lang/String;
         * 9: aload_0
         * 10: aload_2
         * 11: putfield      co/com/claro/portalusuarios/ejb/compartido/ResponseFija.cedula:Ljava/lang/String;
         * 14: return
         *  */
        // </editor-fold>
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    
}

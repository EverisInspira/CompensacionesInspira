/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.mb;

import co.com.claro.compensaciones.dao.CompWebServicesDao;
import co.com.claro.compensaciones.facadelocal.ICompWebservicesFacadeLocal;
import co.com.claro.compensaciones.util.Constants;
import co.com.claro.compensaciones.util.Utils;
import co.com.claro.compensaciones.ws.cliente.login.ClienteFija;
import co.com.claro.compensaciones.ws.cliente.login.RequestFija;
import co.com.claro.compensaciones.ws.cliente.login.ResponseFija;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 * Descripcion: clase que realiza funcionalidades de inicio de sesion para un
 * determiando cliente Objetivo: validar autenticación de un cliente
 *
 * @author Carlos Adrian Andrade
 * @version 04/09/2018
 */
@ManagedBean(name = "inicioSesionMb")
@SessionScoped
public class InicioSesionMb implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Usuario.
     */
    private String usuario;
    /**
     * Clave.
     */
    private String clave;
    /**
     * info
     */
    private String info;

      /**
     * Autenticado.
     */
    private Boolean autenticado = false;
    
    
    @EJB
    public ICompWebservicesFacadeLocal iCompWebservicesFacadeLocal;

    /**
     * Objetivo: Metodo que autentica las credenciales de un determinado cliente
     *
     * @author candradh-everis
     * @version 04/09/2018
     */
    public void autenticar() {
       
        info = "";
        boolean vaidarUsuario = false;
        boolean vaidarPass = false;
        if(usuario == null || (usuario != null && usuario.trim().length() == 0)){
            vaidarUsuario = true;            
        }        
        if(clave == null || (clave != null && clave.trim().length() == 0)){
            vaidarPass = true;          
        }
        
        if(vaidarUsuario && vaidarPass){
            info = Constants.MSJ_ERROR_USER_PASS;
            return;
        }else if(vaidarUsuario){
             info = Constants.MSJ_ERROR_USER;  
             return;
        }else if(vaidarPass){
             info = Constants.MSJ_ERROR_PASS; 
             return;
        }           
        
        Pattern p = Pattern.compile(Constants.EXPRESION_REGULAR_VALIDAR__USER);        
        if(!Utils.evaluateStringInRegularExpression(p, usuario)){
            info = Constants.MSJ_ERROR_CARACTERES_ESPECIALES_USER;
            return;
        }
        
        vaidarUsuario = false;
        vaidarPass = false;
        
        RequestFija request = new RequestFija(usuario, clave);
        URL url = null;
        String wsdl = "";
        try {
            Long estadoServicio = Long.valueOf(
                    Constants.WS_REST_ESTADO_ACTIVO.toString());
            List<CompWebServicesDao> listComp = iCompWebservicesFacadeLocal.
                    findAll(Constants.SERVICIO_LDAP_FIJA, estadoServicio);

            if (listComp != null && !listComp.isEmpty()) {
                CompWebServicesDao objCompWebServicesDao = listComp.get(0);
                wsdl = objCompWebServicesDao.getUrl();
                if (Utils.getStatusUrl(wsdl)) {                  
                    url = new URL(
                            null,
                            wsdl.replace(Constants.WS_REST_PTLU_FIJA_REM_WSDL,
                                    ""),
                            new sun.net.www.protocol.https.Handler()
                    );
                    ClienteFija fija = new ClienteFija();
                    ResponseFija response = fija.consumir(request, url);
                    
                    if(response.getNombre()
                            .equals(Constants.WS_FIJA_RESP_ERROR)){
                        vaidarUsuario = true;
                    }
                    if(response.getCedula()
                            .equals(Constants.WS_FIJA_RESP_ERROR)){
                        vaidarPass = true;
                    }
                    
                    if(vaidarUsuario && vaidarPass){
                        info = Constants.WS_REST_MSJ_ERROR_USER_PASS;
                        return;
                    }else if(vaidarUsuario){
                         info = Constants.WS_REST_MSJ_ERROR_USER;  
                         return;
                    }else if(vaidarPass){
                         info = Constants.WS_REST_MSJ_ERROR_PASS; 
                         return;
                    }              
                    
                    if (!response.getNombre()
                            .equals(Constants.WS_FIJA_RESP_ERROR)
                            && !response.getCedula()
                                    .equals(Constants.WS_FIJA_RESP_ERROR)) {
                        
                         ExternalContext ctx = FacesContext.
                         getCurrentInstance().getExternalContext();                         
                        String ctxPath  = ((ServletContext) ctx.
                                getContext()).getContextPath(); 
                         autenticado = true;
                        ctx.redirect(ctxPath + "/secure/menu/menuPrincipal.xhtml");                        

                    } else {
                        autenticado = false;
                        info = Constants.WS_REST_FIJA_MSJ_ERROR_RESPO;
                    }
                } else {
                    autenticado = false;
                    info = Constants.WS_REST_FIJA_MSJ_ERROR_DISPONIBILIDAD;
                }
            } else {
                autenticado = false;
                info = Constants.WS_REST_MSJ_ERROR_BD;
            }

        } catch (MalformedURLException ex) {
            autenticado = false;
            Logger.getLogger(InicioSesionMb.class.getName()).log(
                    Level.SEVERE, null, ex);
        } catch (Exception ex) {
            autenticado = false;
            Logger.getLogger(InicioSesionMb.class.getName()).log(
                    Level.SEVERE, null, ex);
        }   
        
    }

    /**
     * Objetivo: cerrar sesión Descripción : permite cerrar la sesión
     * redireccionando al login
     *
     * @author candradh - everis
     */
    public void logout() {
        ExternalContext ctx
                = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath
                = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            ((HttpSession) ctx.getSession(false)).invalidate();
            ctx.redirect(ctxPath + "/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(InicioSesionMb.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
    }
    
    
    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the autenticado
     */
    public Boolean getAutenticado() {
        return autenticado;
    }

    /**
     * @param autenticado the autenticado to set
     */
    public void setAutenticado(Boolean autenticado) {
        this.autenticado = autenticado;
    }

}

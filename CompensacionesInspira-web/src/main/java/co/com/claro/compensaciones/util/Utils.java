/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.util;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.json.JSONObject;
import org.w3c.dom.NodeList;

/**
 *
 * @author halmarza
 */
public class Utils {


    public static void logout() {
        ExternalContext ctx
                = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath
                = ((ServletContext) ctx.getContext()).getContextPath();

        try {
            ((HttpSession) ctx.getSession(false)).invalidate();
            ctx.redirect(ctxPath + "/login.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public static void redirectPage(String url) {
        try {
            String urlRedirect = String.valueOf(FacesContext
                    .getCurrentInstance().getExternalContext()
                    .getRequestContextPath());
            urlRedirect += url;
            FacesContext.getCurrentInstance().getExternalContext()
                    .redirect(urlRedirect);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @amendega @description Recarga la pagina en el cual se encuentra el
     * contexto
     * @throws IOException
     */
    public static void reload() throws IOException {
        ExternalContext context
                = FacesContext.getCurrentInstance().getExternalContext();
        context.redirect(((HttpServletRequest) context.getRequest()).getRequestURI());
    }

    /**
     * Objetivo: Metodo que valida una cadena con una Expresion Regular (RE)
     *
     * @author:asantacb-everis 05-04-2018
     * @param pattern - Expresion regular para validar String
     * @param text - String a validar con la Expresion regular 'patron'
     * @return <code>true</code> si la cadena concuerda a la RE y
     * <code>false</code> si no concuerda
     */
    public static boolean evaluateStringInRegularExpression(
            Pattern pattern,
            String text) {
        Matcher mtch = pattern.matcher(text);
        return mtch.matches();
    }

    /**
     * Objetivo: Metodo para verificar si una URL esta o no disponible
     *
     * @author asantacb-everis
     * @version 19/06/2018
     * @param url URL a validar
     * @return <code>true</code> Si URL esta disponible y <code>false</code> si
     * no esta disponible
     * @throws IOException
     */
    public static boolean getStatusUrl(String url) throws IOException {

        try {
            URL urlObj;
            int code = 0;

            if (url.startsWith("http:")) {
                HttpURLConnection con;
                urlObj = new URL(null, url, new sun.net.www.protocol.http.Handler());
                con = (HttpURLConnection) urlObj.openConnection();
                con.setRequestMethod(Constants.GET_STATUS_URL_HTTP_METHOD);
                con.setConnectTimeout(Constants.GET_STATUS_URL_TIMEOUT);
                code = con.getResponseCode();
            } else {
                /*
                Objetivo: PORCION DE CODIGO NECESARIA PARA PERMITIR
                CERTIFICADOS NO VALIDOS
                halmarza-everis
                29/08/18
                 */
 
                javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                        new javax.net.ssl.HostnameVerifier() {
                    @Override
                    public boolean verify(
                            String hostname,
                            javax.net.ssl.SSLSession sslSession
                    ) {
                        return true;
                    }
                });
                TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        @Override
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs,
                                String authType
                        ) {
                        }

                        @Override
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs,
                                String authType
                        ) {
                        }
                    }
                };
                try {
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                } catch (NoSuchAlgorithmException | KeyManagementException e) {
                    Logger.getLogger(Utils.class.getName())
                            .log(Level.SEVERE, null, e);
                }
                /* --- FIN DE PORCION DE CODIGO NECESARIA PARA PERMITIR CERTIFICADOS NO VALIDOS --- */
                urlObj = new URL(null, url, new sun.net.www.protocol.https.Handler());
                HttpsURLConnection con = (HttpsURLConnection) urlObj.openConnection();
                con.setRequestMethod(Constants.GET_STATUS_URL_HTTP_METHOD);
                con.setConnectTimeout(Constants.GET_STATUS_URL_TIMEOUT);
                code = con.getResponseCode();
            }

            if (code == Constants.GET_STATUS_URL_CODE_200) {
                return true;
            }
        } catch (IOException e) {
            Logger.getLogger(Utils.class.getName())
                    .log(Level.SEVERE, null, e);
        }
        return false;
    }

    /**
     * Objetivo: Este metodo convierte cualquier objeto en un JSONObject
     *
     * @author asantacb-everis
     * @version 31/07/2018
     * @param objetoConvertir - Objeto que se quiere convertir
     * @return - JSONObject resultante
     */
    public static JSONObject convertObjectToJSONObject(
            Object objetoConvertir
    ) {
        JSONObject requestJson = new JSONObject(objetoConvertir);
        return requestJson;
    }

    /**
     * Objetivo: Este metodo verifica si por lo menos uno de los valores del
     * array A[i] es null
     *
     * @author asantacb-everis
     * @version 09/08/2018
     * @param values - ArrayList que contiene los valores a verificar
     * @return <code>true</code> si almenos un valor es nulo o
     * <code>false</code> si ningun valor es nulo
     */
    public static boolean checkIfArrayValueIsNull(ArrayList<?> values) {

        for (int i = 0; i < values.size(); i++) {
            if (values.get(i) == null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Objetivo: Este metodo pretende encriptar un string haciendo uso del
     * algoritmo SHA1
     *
     * @param str - string a encriptar
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String encryptStringInSha1(String str)
            throws NoSuchAlgorithmException {
        String hash = "";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.update((df.format(new Date()) + str.trim()).getBytes());
        byte[] encrypt = md.digest();

        for (byte byteTmp : encrypt) {
            int b = byteTmp & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }
            hash += Integer.toHexString(b);
        }
        return hash;
    }

    /**
     * @autor imartipe-Everis
     * @param str
     * @return
     */
    public static String getPortalUsuarioBase64(String str) {
        return Base64.encode(str.getBytes());
    }

    /**
     * @autor imartipe-Everis
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String getPortalUsuarioSHA1(String password)
            throws NoSuchAlgorithmException {
        String hash = "";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.update((df.format(new Date()) + password.trim()).getBytes());
        byte[] encrypt = md.digest();

        for (byte byteTmp : encrypt) {
            int b = byteTmp & 0xff;
            if (Integer.toHexString(b).length() == 1) {
                hash += "0";
            }

            hash += Integer.toHexString(b);
        }

        return hash;
    }

    /**
     * @autor imartipe-Everis
     * @param request
     * @param nombreTag
     * @return
     * @throws SOAPException
     */
    public static String getSoapRpcValue(SOAPMessage request, String nombreTag)
            throws SOAPException {
        NodeList user = request.getSOAPBody()
                .getElementsByTagName(nombreTag);

        if (user.getLength() > 0) {
            return ((Node) (user.item(0))).getValue();
        }

        return null;
    }

    @SuppressWarnings("UnusedAssignment")
    public static String getToken(String nombre, String numeroIdentificacion) {
        try {
            String token = null;
            token = nombre + "." + numeroIdentificacion + "." + new Date() + "." + Math.random();
            token = getPortalUsuarioSHA1(token);
            return token;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * Objetivo: Obtener las propiedades del Bean Descripción: método que
     * permite obtener las propiedades de un Bean especifico
     *
     * @author candrah - everis
     * @param <T>
     * @param name
     * @return
     */
    public static <T extends Object> T getBean(String name) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (T) facesContext.getApplication().getELResolver()
                .getValue(facesContext.getELContext(), null, name);
    }

}

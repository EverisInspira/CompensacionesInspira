package co.com.claro.compensaciones.ws.cliente.login;

import co.com.claro.compensaciones.util.Constants;
import co.com.claro.compensaciones.util.Utils;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

/**
 *
 * @author imartipe-Everis
 */
public class ClienteFija {

    @SuppressWarnings({"null", "UseSpecificCatch", "UnusedAssignment"})
    public ResponseFija consumir(RequestFija request, URL url)
            throws Exception {
        OutputStream reqOutStream = null;
        SOAPMessage responseSoap = null;
        InputStream responseSoapRpc = null;
        BufferedReader rd = null;
        String responseXml = null;
        String requestStrXml = null;
        StringBuilder responseStrB = null;

        try {
            SSLContext sslContext = SSLContext.
                    getInstance(Constants.WS_REST_PTLU_SSL_SALTAR);
            sslContext.init(new KeyManager[0], new TrustManager[]{new 
        ClienteFija.DefaultTrustManager()}, new SecureRandom());
            SSLContext.setDefault(sslContext);

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.
                    getSocketFactory());

            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            }
            );

            HttpsURLConnection conn = (HttpsURLConnection) 
                    url.openConnection();
            conn.setRequestMethod(Constants.WS_REST_PTLU_SOAP_FIJA_POST);
            conn.setDoOutput(true);
            conn.setAllowUserInteraction(true);
            conn.setRequestProperty(
                    Constants.WS_REST_PTLU_SOAP_FIJA_CONTENT_TYPE,
                    Constants.WS_REST_PTLU_SOAP_FIJA_TYPE_MIME
            );
            conn.setRequestProperty(
                    Constants.WS_REST_PTLU_SOAP_FIJA_ACTION_PROPTR,
                    Constants.WS_REST_PTLU_SOAP_FIJA_ACTION_METODO
            );

            requestStrXml = Constants.WS_REQUEST_XML_FIJA;

            requestStrXml = requestStrXml.replace(
                    Constants.WS_REST_PTLU_SOAP_FIJA_REQ_USER, Utils
                    .getPortalUsuarioBase64(request.getUsuario()))
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_REQ_PASS, Utils
                            .getPortalUsuarioBase64(request.getPassword()))
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_REQ_HASH, Utils
                            .getPortalUsuarioSHA1(request.getPassword()));

            reqOutStream = conn.getOutputStream();
            reqOutStream.write(requestStrXml.getBytes());
            reqOutStream.flush();

            rd = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            responseStrB = new StringBuilder();

            while ((responseXml = rd.readLine()) != null) {
                responseStrB.append(responseXml);
            }

            String responseStr = responseStrB.toString()
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_F1,
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_R1)
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_F2,
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_R2)
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_F3,
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_R3)
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_F4,
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_R4)
                    .replace(Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_F5,
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_REP_R5);

            responseSoapRpc = new ByteArrayInputStream(responseStr.getBytes());
            responseSoap = MessageFactory
                    .newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).
                    createMessage(null, responseSoapRpc);

            rd.close();
            responseSoapRpc.close();
            reqOutStream.flush();
            reqOutStream.close();
            conn.disconnect();

            ResponseFija responseFija = new ResponseFija(
                    Utils.getSoapRpcValue(responseSoap, 
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_NOMBRE),
                    Utils.getSoapRpcValue(responseSoap, 
                            Constants.WS_REST_PTLU_SOAP_FIJA_RES_CEDULA)
            );

            return responseFija;
        } catch (IOException | KeyManagementException | 
                NoSuchAlgorithmException |
                SOAPException ex) {
            Logger.getLogger(ClienteFija.class.getName()).log(
                    Level.SEVERE, null, ex);
            throw new Exception(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(ClienteFija.class.getName()).log(
                    Level.SEVERE, null, ex);
            throw new Exception(ex.getMessage());
        } finally {
            rd.close();
            responseSoapRpc.close();
            reqOutStream.flush();
            reqOutStream.close();
        }
    }

    /**
     * @autor imartipe-Everis
     */
    private static class DefaultTrustManager
            implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}

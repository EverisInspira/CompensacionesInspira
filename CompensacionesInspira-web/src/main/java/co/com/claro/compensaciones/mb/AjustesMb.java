/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.claro.compensaciones.mb;

import co.com.claro.compensaciones.dao.CompAjustesDao;
import co.com.claro.compensaciones.facadelocal.ICompAjustesFacadeLocal;
import co.com.claro.compensaciones.util.Constants;
import co.com.claro.compensaciones.util.Utils;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Descripcion: clase que realiza funcionalidades de paametrización sobre 
 * los ajustes de compensaciones 
 *
 * @author vcastril-@everis
 * @version 04/09/2018
 */
@ManagedBean(name = "ajustesMb")
@ViewScoped
public class AjustesMb implements Serializable{
   
    private static final long serialVersionUID = 1L;
    
    /**
     * Codigo Ajuste
     */
    private String codAjuste;
    /**
     * Codigo Ajuste
     */
    private String usuario;
    /**
     * Ajuste
     */
    private CompAjustesDao ajuste;
    /**
     * Descripcion 
     */
    private String descripcion;
    /**
     * Estado 
     */
    private String estado;
     /**
     * Estado 
     */
    private String usuarioLogueado;
    /**
     * Información 
     */
    private String info;
    /**
     * lista de compenaciones ajustes 
     */
    private List<CompAjustesDao> listTabla;
    /**
     * Compensación Ajuste 
     */
    private CompAjustesDao compAjustesDao= null;

    /**
     * 
     */
    @EJB
    public ICompAjustesFacadeLocal compAjustesFacadeLocal;

    /**
     * Constructor de la clase ajustes 
     */
    @PostConstruct
    public void init() {  
        try {
            setListTabla(compAjustesFacadeLocal.findAll());
            InicioSesionMb inicioSesionMb
                = Utils.getBean("inicioSesionMb");
            usuarioLogueado = inicioSesionMb.getUsuario();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AjustesMb.class.getName())
                    .log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Descripcion: limpia los campos de la clase ajustes  
     *
     * @author vcastril-@everis
     */
    public void nuevo(){
        codAjuste = null;
        descripcion = null;
        estado = null;
    }
    /**
     * Descripcion: actualizar los campos de la parametrización de ajustes 
     *
     * @author vcastril-@everis
     * @throws Exception 
     */
    public void actualizar() throws Exception{
        boolean validacion = validarCampos();
        if(validacion){
            compAjustesDao.setCodAjuste(codAjuste);
            compAjustesDao.setDescripcion(descripcion);
            compAjustesDao.setEstado(Long.parseLong(estado));
            compAjustesDao.setUsuario(usuarioLogueado);
            compAjustesFacadeLocal.edit(compAjustesDao);
        }
        nuevo();
        setListTabla(compAjustesFacadeLocal.findAll());
    }
    /**
     * Descripcion: guaradar los campos de la parametrización de ajustes 
     *
     * @author vcastril-@everis
     * @throws Exception 
     */
    public void guardar() throws Exception{
        boolean validacion = validarCampos();
        boolean validacionCodigoDescrion = validarCodigo();
        
        compAjustesDao = new CompAjustesDao();
        if(validacion && validacionCodigoDescrion){
            compAjustesDao.setCodAjuste(codAjuste);
            compAjustesDao.setDescripcion(descripcion);
            compAjustesDao.setEstado(Long.parseLong(estado));
            compAjustesDao.setUsuario(usuarioLogueado);
            compAjustesFacadeLocal.create(compAjustesDao);
        }
        nuevo();
        setListTabla(compAjustesFacadeLocal.findAll());
    }
    
    /**
     * Descripcion: eliminar los campos de la parametrización de ajustes 
     *
     * @author vcastril-@everis
     * @throws Exception 
     */
    public void delete() throws Exception{
        if(ajuste != null && ajuste.equals("")){
            compAjustesFacadeLocal.remove(ajuste);
        }
        setListTabla(compAjustesFacadeLocal.findAll());
    }
    /**
     * Descripcion: Seleccionar los campos de la parametrización de ajustes 
     *
     * @author vcastril-@everis
     * @param ajustesDao 
     */
    public void preEditar(CompAjustesDao ajustesDao){
        codAjuste = ajustesDao.getCodAjuste();
        descripcion = ajustesDao.getDescripcion();
        estado = String.valueOf(ajustesDao.getEstado());
        compAjustesDao = ajustesDao;
    }
    /**
     * Descripcion: Validar campos obligatorios de la parametrización de ajustes 
     *
     * @author vcastril-@everis
     * @return 
     */
    public boolean validarCampos(){
        if(codAjuste!= null && !codAjuste.equals("")){
            if(descripcion!= null && !descripcion.equals("")){
                if(estado!= null && !estado.equals("")){
                    return true;
                }else{
                    info = Constants.INGRESE_ESTADO;
                }  
            }else{
                info = Constants.INGRESE_DESCRIPCION;
            }
        }else{
            info = Constants.INGRESE_CODIGO;
        }
        return false;
    }
    
    public boolean validarCodigo(){
        List<CompAjustesDao> compAjustesCodigo  = compAjustesFacadeLocal.
                findCodigo(codAjuste);
        List<CompAjustesDao> compAjustesDescripcion  = compAjustesFacadeLocal.
                findDescripcion(descripcion);
        return compAjustesCodigo.isEmpty() && compAjustesDescripcion.isEmpty();
    }

    /**
     * 
     * @return the codAjuste
     */
    public String getCodAjuste() {
        return codAjuste;
    }

    /**
     * 
     * @param codAjuste 
     */
    public void setCodAjuste(String codAjuste) {
        this.codAjuste = codAjuste;
    }

    /**
     * 
     * @return ajuste
     */
    public CompAjustesDao getAjuste() {
        return ajuste;
    }

    /**
     * 
     * @param ajuste 
     */
    public void setAjuste(CompAjustesDao ajuste) {
        this.ajuste = ajuste;
    }

    /**
     * 
     * @return descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * 
     * @param descripcion 
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * 
     * @return estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * 
     * @param estado 
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * 
     * @return info
     */
    public String getInfo() {
        return info;
    }

    /**
     * 
     * @param info 
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * 
     * @return listTabla
     */
    public List<CompAjustesDao> getListTabla() {
        return listTabla;
    }

    /**
     * 
     * @param listTabla 
     */
    public void setListTabla(List<CompAjustesDao> listTabla) {
        this.listTabla = listTabla;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    
    
}

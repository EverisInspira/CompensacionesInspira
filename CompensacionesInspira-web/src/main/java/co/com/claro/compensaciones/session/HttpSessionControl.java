package co.com.claro.compensaciones.session;

import co.com.claro.compensaciones.mb.InicioSesionMb;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @Objetivo: controlar la sesion 
 * @Descripción: clase que permite contener métodos que controlan lo del inicio
 * de sesión 
 * 
 * @author candradh- everis
 * @Versión: 1.0
 */
@WebListener
public class HttpSessionControl implements HttpSessionListener {
    
  

    public HttpSessionControl() {
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
    }

    /**
     * @Objetivo: sesión destruida
     * @Descripción: metodo que permite volver cambiar variables para controlar 
     * el inicio de sesión 
     * 
     * @author candradh - everis
     * @param se 
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        if (se.getSession() != null) {
            InicioSesionMb inicioSesionBean
                    = (InicioSesionMb) se.getSession()
                    .getAttribute("inicioSesionMb");
            if (inicioSesionBean.getUsuario() != null && 
                    inicioSesionBean.getAutenticado()){
                try {
                   inicioSesionBean.setAutenticado(Boolean.FALSE);
                   inicioSesionBean.setUsuario(null);
                   inicioSesionBean.setClave(null);
                } catch (Exception ex) {
                    Logger.getLogger(HttpSessionControl.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
    }

}

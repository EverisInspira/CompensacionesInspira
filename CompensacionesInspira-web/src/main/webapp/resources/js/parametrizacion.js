/*
 * imartipe@everis
 */
/* global modalOperationProgress, modalEditEnd, jsf, modalEdit, modalDeleteEnd, modal, modalCreateEnd, modalError, data */
//Menú vertical
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    //document.getElementById("contenido").style.marginLeft = "250px";
    /*
     * cgordilg 19-01-2018
     */
    $(document).ready(function () {
        $(".fadeDiv").show('swing');
        $("#navMenu").show('swing');
        $(".closebtn").show('swing');
        $('#mySidenav').show();
        $('.dataTables_wrapper').css({"z-index": "-1"});
        $('.glyphicon-calendar').css({position: 'relative', "color": "white"});
    });
    /*
     *  Fin cgordilg 19-01-2018
     */
}

function closeNav() {
    /*
     * cgordilg 19-01-2018
     */
    $(document).ready(function () {
        $(".fadeDiv").hide('swing');
        $("#navMenu").hide('swing');
        $(".closebtn").hide('swing');
        $('#mySidenav').hide();
        $('.dataTables_wrapper').css({"z-index": "0"});
        $('.glyphicon-calendar').css({"color": "black"});

    });
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("contenido").style.marginLeft = "0";
    document.body.style.backgroundColor = "white";
    /*
     *  Fin cgordilg 19-01-2018
     */
}

function openBackGround()
{
    $(document).ready(function () {
        $(".fadeDiv").show('swing');
        $('.dataTables_wrapper').css({"z-index": "-1"});
    });
}

function hideBackground()
{
    $(document).ready(function () {
        $(".fadeDiv").hide('swing');
        $('.dataTables_wrapper').css({"z-index": "0"});
    });
    document.body.style.backgroundColor = "white";
}

//-----------
function parametrizacionDialogModal(idObj, title, fnClose)
{
    $(idObj).dialog
            (
                    {
                        modal: true,
                        title: title,
                        resizable: false,
                        buttons:
                                {
                                    Cerrar: function ()
                                    {
                                        if (fnClose !== undefined)
                                            fnClose();
                                        $(this).dialog("destroy");
                                    }
                                }
                    }
            );
}

/**
 * 
 * @param {type} idObj
 * @param {type} title
 * @param {type} fnClose
 * @returns {undefined}
 * acastanv@everis
 */
function opsDialogModalReload(idObj, title, fnClose)
{
    $(idObj).dialog
            (
                    {
                        modal: true,
                        title: title,
                        resizable: false,
                        buttons:
                                {
                                    Cerrar: function ()
                                    {
                                        if (fnClose !== undefined)
                                            fnClose();
                                        window.location.reload();
                                        $(this).dialog("destroy");
                                    }
                                }
                    }
            );
}

/**
 * 
 * @param {type} idObj
 * @param {type} title
 * @param {type} fnClose
 * @returns {undefined}
 * cgordilg 24-01-2018
 */
function opsDialogModalEdit(idObj, title, fnClose)
{

    $(idObj).dialog
            (
                    {
                        modal: true,
                        title: title,
                        resizable: false,
                        width: 1400,
                        height: 360,
                        buttons:
                                {
//                Cancelar: function ()
//                {
//                    if(fnClose !== undefined)
//                        fnClose();
//                    /*window.location.reload();*/
//                    $(this).dialog("destroy");
//                }
                                }
                    }
            );
    $(idObj).siblings('div.ui-dialog-titlebar').remove();
    $(idObj).siblings('.ui-widget-overlay').css({"border": "4px solid #a71010"});
}

//$(window).load(function () {
// jsf.ajax.addOnEvent(function(data) {
//  if (data.status === 'success') {
//      $.datepicker.setDefaults({
//        dateFormat: "dd/mm/yy"
//    });
//       $(".Fecha").datepicker({
//             dateFormat: "dd/mm/yy"
//        });     
//        }
// });
//});

function habilitarCalendar(id)
{
    jQuery(document).ready(function ($) {
//        $("#" + id).datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//        $(".Fecha").datepicker({
//            dateFormat: "dd/mm/yy"}).val();  

        $(document).on('mouseover', '.Fecha', function () {
            $(this).datepicker({
                dateFormat: "dd/mm/yy"
            });
        });

//        $("#FechaInicioL").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//
//        $("#FechaVenceL").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//
//        $("#ingresoL").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
////        dserrato
//        $("#FechaInicio").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//        $("#FechaVence").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//        $("#FechaInicioEd").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//        $("#FechaVenceEd").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//        $("#FechaInicioEd").datepicker({
//            dateFormat: "dd/mm/yy"}).val();
//        $("#FechaVenceEd").datepicker({
//            dateFormat: "dd/mm/yy"}).val();

    });
}

function initFecha()
{
    jQuery(document).ready(function ($) {
        $("#fecInicio").datepicker({
            dateFormat: "dd/mm/yy"}).val();

        $("#fecFin").datepicker({
            dateFormat: "dd/mm/yy"}).val();

    });
}


function opsDialogModalRemove(idObj, title, fnClose)
{
    $(idObj).dialog
            (
                    {
                        modal: true,
                        title: title,
                        resizable: false,
                        width: 400,
                        height: 250,
                        buttons:
                                {
                                }
                    }
            );
    $(idObj).siblings('div.ui-dialog-titlebar').remove();
    $(idObj).siblings('.ui-widget-overlay').css({"border": "4px solid #a71010"});
}

function cerrarPopUp(idObj, title, fnClose)
{
    $(idObj).dialog
            (
                    );
    if (fnClose !== undefined)
        fnClose();
    /*window.location.reload();*/
    $(idObj).dialog("destroy");
    if (idObj)
        hideBackground();
}
/*
 * Fin cgordilg 24-01-2018
 */

function parametrizacionDialogModalLoading(idObj, title)
{
    $(idObj).dialog
            (
                    {
                        modal: true,
                        title: title,
                        closeOnEscape: false,
                        draggable: false,
                        resizable: false,
                        open: function (event, ui)
                        {
                            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                        }
                    }
            );
}

function allCheck(idCheck, estado)
{
    if (estado === undefined)
    {
        estado = $(idCheck).prop('checked');
    }

    $(':checkbox').prop('checked', estado);
}

function selectAllCheck(idCheck)
{
    allCheck(idCheck, true);
}

function unSelectAllCheck(idCheck)
{
    allCheck(idCheck, false);
}

function documentRun(fnRun)
{
    $(document).ready
            (
                    fnRun
                    );
}

/*imartipe - stackoverflow*/
function checkWithValue(val, strSeparator, fnClear)
{
    if (strSeparator !== undefined)
    {
        arr = val.split(strSeparator);

        $.each
                (
                        arr,
                        function (index, value)
                        {
                            $(":checkbox").filter
                                    (
                                            function ()
                                            {
                                                return this.value === value;
                                            }
                                    ).prop("checked", "true");
                        }
                );
    } else
    {
        $(":checkbox").filter
                (
                        function ()
                        {
                            return this.value === val;
                        }
                ).prop("checked", "true");
    }

    if (fnClear !== undefined)
        fnClear;
}

//imartpe@everis.com
function listBoxGetAllItem(idListBox)
{
    return $(idListBox + ' option').map
            (
                    function ()
                    {
                        return this.value;
                    }
            ).get();
}

//imartpe@everis.com
function listBoxExistValue(idListBox, valOption)
{
    if (valOption == "")
        return false;
    return 0 !== $(idListBox + ' option[value=' + valOption + ']').length;
}

//imartipe
function listBoxCountItems(idListBox)
{
    return $(idListBox + ' option').length;
}

function listBoxSelectItem(idListBox, index)
{
    if (index === undefined)
        $(idListBox)[0].selectedIndex = 0;
    else
        $(idListBox)[0].selectedIndex = index;
}

function listBoxAddItem(idListBox, data)
{
    $(idListBox).append(data);
}

function listBoxDelSelectItem(idListBox)
{
    $(idListBox + ' option:selected').remove();
}

/*
 * imartipe@everis.com
 * Ayuda a escribir los menus
 **/
function docAdd(text)
{
    document.writeln(text);
}

/**
 * 
 * @param {string} idMenu
 * @param {optional string} idDiv
 * @returns {mNew} El elemento para trabajar con el menu y agregar items
 */
function mNew(idMenu, idDiv)
{
    this.idDiv = idDiv;
    this.menuId = "#" + idMenu;
    docAdd("<div " + ((idDiv !== undefined) ? "id='" + idDiv + "'" : "") + ">");
    docAdd("<ul class='menuParametrizacion' id='" + idMenu + "'>");
    docAdd("</ul>");
    docAdd("</div>");

    return this;
}

/**
 * 
 * @param {mNew|string} menu The return value of {mNew} or {id} of one submenu
 * @param {string} text Text of menu item
 * @param {optional string} idSubMenu Set for create submenu
 * @param {optional string} url path page
 * @returns {undefined|null} Nothing
 * 
 * Si se llama solo con 2 parametros crea un menu horizontal.
 * Si se llama con 3 parametros crea un menu horizontal pero preaparado
 * para tener submenus o subniveles. El 3 parametro se usa como id para
 * identificar al submenu padre.
 * 
 * Ej.  objMenu = mNew('idDelMenu');
 *          Crea un objeto menu con id '#idDelMenu'
 *      mAddM(objMenu, 'TextoMenu');
 *          Crea un menu horizontal con texto 'TextoMenu', no soporta submenu
 *      mAddM(objMenu, 'TextoMenu', 'idSubMenuPadre');
 *          Crea un menu horizontal con texto 'TextoMenu', soporta submenu,
 *          el submenu se identificara con el id '#idSubMenuPadre'
 *      mAddM('#idSubMenuPadre', 'TextoSubMenu');
 *          Agrega al menu '#idSubMenuPadre' con soporte de submenu el sebmenu
 *          con texto 'TextoSubMenu'
 *      mAddM(objMwnu, 'TextoSubMenu', null , 'path.xhtml');
 *          Crea un menu horizontal, sin soporte de submenu, va a la pagina
 *          'path.xhtml' al dar click sobre el menu
 *      mAddM('#idSubMenuPadre', 'TextoMenu', null , 'path2.xhtml');
 *          Agrega un submenu al menu '#idSubMenuPadre' con texto 'TextoMenu'
 *          al dar click va a la pagina 'path2.xhtml'
 * 
 */
function mAddM(menu, text, idSubMenu, url)
{
    (($.type(menu) === "string") ? idElemt = menu : idElemt = menu.menuId);

    $(idElemt)
            .append
            (
                    "<li><a href='"
                    + ((url === undefined) ? "#" : url)
                    + "'>"
                    + text
                    + "<span class='menuIParametrizacion'>&#9660;</span></a>"
                    + ((idSubMenu !== undefined && idSubMenu !== null) ? "<ul id='" + idSubMenu + "'></ul>" : "")
                    + "</li>"
                    );
}

function mAddMJsf(menu, text, idSubMenu, idBtnJsfAction)
{
    (($.type(menu) === "string") ? idElemt = menu : idElemt = menu.menuId);

    $(idElemt)
            .append
            (
                    "<li><a href='#' onclick=\"$('" + idBtnJsfAction + "').click()\">"
                    + text
                    + "<span class='menuIParametrizacion'>&#9660;</span></a>"
                    + ((idSubMenu !== undefined && idSubMenu !== null) ? "<ul id='" + idSubMenu + "'></ul>" : "")
                    + "</li>"
                    );
}

/**
 * @autor imartipe
 * @param {type} data
 * @returns {undefined}
 */
function estadoSolicitudAjax(data)
{

    estado = data.status;
    switch (estado)
    {
        case "begin":
            document.getElementById("popUpProgress").style.display = "block";
            break;
        case "success":
            document.getElementById("popUpProgress").style.display = "none";
            break;
    }
}

/*function downloadTemplate(idLink, objCall)
{
    var a = document.getElementById(idLink);
    var listType = null;
    const path = '../../main/resources/co.com.claro.ListaVerficacion/cargueMasivoPlatillas/XXX.csv';
    var pathNew = null;
    var listTypeSelected = null;
    a.href = path;
    a.download = "TemplateClientes.csv";

    if (objCall !== undefined)
    {
        listType = document.getElementById(objCall);
        listTypeSelected = $('#' + objCall + ' option:selected').val();

        if (listTypeSelected == 2)
        {
            pathNew = path.replace("XXX", "TemplateClientes");
            a.download = "TemplateClientes.csv";
        } else if (listTypeSelected == 3)
        {
            pathNew = path.replace("XXX", "TemplateTelefono");
            a.download = "TemplateTelefono.csv";
        } else if (listTypeSelected == 1)
        {
            pathNew = path.replace("XXX", "TemplateCheques");
            a.download = "TemplateCheques.csv";
        } else if (listTypeSelected == 4)
        {
            pathNew = path.replace("XXX", "TemplateCorreos");
            a.download = "TemplateCorreos.csv";
        }
    } else
    {
        pathNew = path.replace("XXX", "TemplateClientes");
        a.download = "TemplateClientes.csv";
    }

    a.href = pathNew;
}*/

//imartpe
function loadingWindow()
{
    jsf.ajax.addOnEvent(estadoSolicitudAjax);
}

function loadWindow() {
    /**
     * 
     * cgordilg 23-01-2018
     */
    asignarFechaActual();
    $(document).ready(function ()
    {
        $('.dataTables_wrapper').css({"z-index": "0"});
    });

    /**
     * 
     * Fin cgordilg 23-01-2018
     */
    jsf.ajax.addOnEvent(estadoSolicitudAjax);
}

/**
 * 
 * cgordilg 23-01-2018
 */
function asignarFechaActual()
{
    var fecha = new Date();
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
            "Diciembre");
    var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves",
            "Viernes", "Sábado");


    var hours = fecha.getHours();
    var minutes = fecha.getMinutes();
    var dn = "A.M";
    if (hours > 12) {
        dn = "P.M";
        hours = hours - 12;
    }
    if (hours == 0)
        hours = 12;
    if (minutes <= 9)
        minutes = "0" + minutes;

    var fechaActual = (diasSemana[fecha.getDay()] + ", " + fecha.getDate() + " de " +
            meses[fecha.getMonth()] + " de " + fecha.getFullYear());
    var horaActual = (hours + " : " + minutes + " " + dn);
    $(document).ready(function () {
        var date = document.getElementById("dateSys");
        date.textContent = fechaActual;
        var hour = document.getElementById("dateTimeAct");
        hour.textContent = horaActual;
    });
    setTimeout('asignarFechaActual()', 1000);
}

/*
 /**
 * 
 * Fin cgordilg 23-01-2018
 */

/**
 * 
 * INI eloaizao 22-03-2018
 */
/*
 function callbackEdit() {
 $(document).ready(function () {
 $("#btnActualizar").bind("click", function () {
 modalEdit.style.display = "none";
 jsf.ajax.addOnEvent(cargarEditExitoso);
 jsf.ajax.addOnError(cargarEditExitoso);
 });
 });
 }
 function callbackRemove() {
 $(document).ready(function () {
 $("#btnEliminar").bind("click", function () {
 modal.style.display = "none";
 jsf.ajax.addOnEvent(cargarRemoveExitoso);
 jsf.ajax.addOnError(cargarRemoveExitoso);
 });
 });
 }
 
 function callbackInsertar() {
 $(document).ready(function () {
 $("#btnInsertar").bind("click", function () {
 jsf.ajax.addOnEvent(cargarCreateExitoso);
 jsf.ajax.addOnError(cargarCreateExitoso);
 });
 });
 }
 
 function cargarCreateExitoso(data) {
 estado = data.status;
 modalOperationProgress.style.display = "none";
 if (estado === "serverError") {
 modalError.style.display = "block";
 return mayFireAjax(this);
 }
 if (estado === "success") {
 modalCreateEnd.style.display = "block";
 }
 }
 
 function cargarRemoveExitoso(data)
 {
 estado = data.status;
 if (estado === "serverError") {
 modalError.style.display = "block";
 return mayFireAjax(this);
 }
 if (estado === "success") {
 modalDeleteEnd.style.display = "block";
 }
 }
 
 function cargarEditExitoso(data)
 {
 estado = data.status;
 if (estado === "serverError") {
 modalError.style.display = "block";
 return mayFireAjax(this);
 }
 if (estado === "success") {
 modalEditEnd.style.display = "block";
 }
 }
 
 function cerrarEditExitoso() {
 $(document).ready(function () {
 $("#iconEditEx").bind("click", function () {
 location.reload();
 });
 });
 }
 */

function cerrarCrearExitoso() {
    $(document).ready(function () {
        $("#iconCreateEx").bind("click", function () {
            location.reload();
        });
    });
}


function cerrarRemoveExitoso() {
    $(document).ready(function () {
        $("#iconDeleteEx").bind("click", function () {
            location.reload();
        });
    });
}

function cerrarError() {
    $(document).ready(function () {
        $("#iconErrorEx").bind("click", function () {
            location.reload();
        });
    });
}

function cargarArchivo()
{
    var jsfcargueField = document.getElementById("loadFile");
    jsfcargueField.click();
}

function carga()
{
    $('input[name=loadFile]').change(function (ev)
    {
        var val = $(this).val();
        document.getElementById("cmTxtxPlan").innerHTML = 'Completado';
        // your code
    });
}

/**
 * 
 * FIN eloaizao 22-03-2018
 */


/**
 *
 * FIN eloaizao 22-03-2018
 */

/**** Validadores ****/

/**
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * @author asantacb-everis
 * 16/04/18
 * Accion: Funcion para validar que el campo solo contenga Numeros
 */
//<![CDATA[
function ValidatorOnlyNumbers(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");
    var patron = /^\d*$/;
    if (valor.search(patron))
    {
        mensaje.innerHTML = "Solo se permiten numeros";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
    }
}
//]]>



/**
 * @author asantacb-everis
 * 16/04/18
 * Accion: Funcion para validar campos obligatorios
 * @param {type} id
 * @param {type} idValueArray
 * @returns {undefined}
 */

//<![CDATA[

function ObligatoryField(id, idValueArray)
{
    var FielsNull = false;
    for (i = 0; i < idValueArray.length; i++)
    {

        if (idValueArray[i].toString() === "")
        {
            FielsNull = true;
            document.getElementById(id).disabled = true;
            document.getElementById(id).style.backgroundColor = "#5d5353";
            var mensaje = document.getElementById(id + "_mensaje");
            mensaje.innerHTML = "Los campos marcados con(*) son obligatorios";
            mensaje.style.display = "block";

            break;
        }
    }

    if (!FielsNull)
    {
        document.getElementById(id).disabled = false;
    }
}

function validateDocumentValue(id, type, value)
{
    var patron = /^\d*$/;
    var alphanumericPatron = /^[a-zA-Z0-9]*$/;
    var val = value;
    var mensaje = document.getElementById(id + "_mensaje");

    if (type === "CC" || type === "CE" || type === "CD" || type === "TI"
            || type === "TE" || type === "RN" || type === "SI") {
        if ((value.length >= 6 || value.length <= 12) || !val.match(patron)) {
            document.getElementById(id).value = "";
            mensaje.innerHTML = "Numero de identificación debe tener entre 6 a 12 dígitos numéricos";
            mensaje.style.display = "block";
        } else {
            mensaje.innerHTML = "";
            mensaje.style.display = "none";
            return true;
        }
    } else if (type === "NI") {
        if ((value.length !== 9) || !val.match(patron)) {
            document.getElementById(id).value = "";
            mensaje.innerHTML = "Numero de NIT debe ser de 9 dígitos numéricos";
            mensaje.style.display = "block";
        } else {
            mensaje.innerHTML = "";
            mensaje.style.display = "none";
            return true;
        }
    } else if (type === "PS") {
        if ((value.length >= 6 || value.length <= 12) || !val.match(alphanumericPatron)) {
            document.getElementById(id).value = "";
            mensaje.innerHTML = "Numero de pasaporte debe tener entre 6 a 12 caracteres";
            mensaje.style.display = "block";
        } else {
            mensaje.innerHTML = "";
            mensaje.style.display = "none";
            return true;
        }
    }
}
//]]>


/**
 * @author asantacb-everis
 * 16/04/18
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * Accion: Funcion para validar que el campo solo contenga numeros y letras
 */
//<![CDATA[
function ValidatorOnlyAlphaNumeric(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");

    if (/[^a-zA-Z0-9]/.test(valor))
    {
        var input = document.getElementById(id);

        do
        {
            input.value = input.value.substring(0, input.value.length - 1);
            if (ValidatorOnlyAlphaNumeric(id, input.value))
            {
                break;
            }
        } while (true)
        mensaje.innerHTML = "Solo se permiten numeros y letras";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}
//]]>

/**
 * @author asantacb-everis
 * 16/04/18
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * Accion: Funcion para validar que el campo solo contenga letras
 */
//<![CDATA[
function ValidatorOnlyLetters(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");
    if (/[^a-zA-Z\ ]/.test(valor))
    {
        var input = document.getElementById(id);

        do
        {
            input.value = input.value.substring(0, input.value.length - 1);

            if (ValidatorOnlyLetters(id, input.value))
            {
                break;
            }
        } while (true)

        mensaje.innerHTML = "Solo se permiten letras";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}
//]]>


/**
 * @author krodrigz- everis
 * 17/05/18
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * Metodo:para validar que el campo contenga letras Mayusculas, numeros, punto y underscore
 */

function ValidatorAlphaNumericDotAndUnderscore(id, valor)
{
    var mensaje = document.getElementById(id + "_mensaje");
    if (/[^A-Z0-9_\ ]/.test(valor))
    {
        var input = document.getElementById(id);
        do
        {
            input.value = input.value.substring(0, input.value.length - 1);

            if (ValidatorOnlyAlphaNumericDotAndUnderscore(id, input.value))
            {
                break;
            }
        } while (true)
        mensaje.innerHTML = "Solo permite numeros, letras Mayusculas, puntos y underscore";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}




/**
 * @author asantacb-everis
 * 16/04/18
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * Accion: Funcion para validar que el campo solo contenga letras, numeros y .
 */
//<![CDATA[
function ValidatorOnlyAlphaNumericAndDot(id, valor)
{
    var mensaje = document.getElementById(id + "_mensaje");

    if (/[^a-zA-Z0-9\.\ ]/.test(valor))
    {
        var input = document.getElementById(id);

        do
        {
            input.value = input.value.substring(0, input.value.length - 1);

            if (ValidatorOnlyAlphaNumericAndDot(id, input.value))
            {
                break;
            }
        } while (true)
        mensaje.innerHTML = "Solo se permiten numeros, letras y '.'";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}
//]]>

/**
 * @author asantacb-everis
 * 16/04/18
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * Accion: Funcion para validar que el campo solo contenga letras, numeros y _
 */
//<![CDATA[
function ValidatorOnlyAlphaNumericAndUnderscore(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");

    if (/[^a-zA-Z0-9\_]/.test(valor))
    {
        var input = document.getElementById(id);

        do
        {
            input.value = input.value.substring(0, input.value.length - 1);

            if (ValidatorOnlyAlphaNumericAndUnderscore(id, input.value))
            {
                break;
            }
        } while (true)
        mensaje.innerHTML = "Solo se permiten numeros, letras y '_'";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}
//]]>


/**
 * @author asantacb-everis
 * 16/04/18
 * @param {type} id
 * @param {type} valor
 * @returns {Boolean}
 * Accion: Funcion para validar el nuevo WSDL cumpla la estructura
 */
//<![CDATA[
function ValidatorWsdlPath(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");

    if (/https{0,1}:\/\/([a-zA-Z0-9\.\_\\\/(]){1,}?.wsdl/.test(valor))
    {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    } else {

        mensaje.innerHTML = "Ejemplo: http://ejemplo.url09/.?wsdl o https://ejemplo.url09/.?wsdl";
        mensaje.style.display = "block";
        return false;
    }
}

/**
 * @author: cgordilg
 
 * @param {type} id
 * @param {type} valor
 * @returns {undefined} */
function ValidatorEmail(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");

    if (!/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/i.test(valor))
    {
        mensaje.innerHTML = "Correo electronico invalido";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}

/**
 * @author: cgordilg
 
 * @param {type} id
 * @param {type} valor
 * @returns {undefined} */
function ValidatorDate(id, valor)
{

    var mensaje = document.getElementById(id + "_mensaje");

    if (!/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/.test(valor))
    {
        mensaje.innerHTML = "Formato de fecha invalido";
        mensaje.style.display = "block";
        return false;
    } else {
        mensaje.innerHTML = "";
        mensaje.style.display = "none";
        return true;
    }
}

//]]>

////<![CDATA[
//function ActivateComponet(id)
//{
//    document.getElementById(id).disabled = false;
//    document.getElementById(id).style.backgroundColor = "#000000";
//    var mensaje = document.getElementById(id + "_mensaje");
//    mensaje.innerHTML = "";
//    mensaje.style.display = "none";
//}
//]]>
// llenar tabla 
function loadDataTable() {
    $(document).ready(
            function () {
                t = $('#table').DataTable();
                t.on(
                        'order.dt search.dt',
                        function () {
                            t.column(0, {
                                search: 'applied',
                                order: 'applied'
                            }).nodes().each(
                                    function (cell, i) {
                                        cell.innerHTML = i + 1;
                                    }
                            );
                        }
                );
                -
                        t.draw();
            }
    );
}

function reload() {
    $(document).ready(function () {
        $(".btnCancelTest").bind("click", function () {
            location.reload();
        });
    });
}

/**
 * @author: edwin
 * ocultla mensajes de validacion
 * click sobre inputs
 * @returns {false}
 *  */
function hiddenMessage() {

    mensajesError = document.getElementsByClassName('mensajes-error');
    for (itemMessaje in mensajesError) {
        mensajesError[itemMessaje].style.display = "none";
    }
    return false;
}

/**
 * @author: edwin
 * muestra  mensajes de validacion
 * @param {type} accion
 * @returns {false}
 *  */
function showMessage() {
    mensajesError = document.getElementsByClassName('mensajes-error');
    for (itemMessaje in mensajesError) {
        mensajesError[itemMessaje].style.display = "block";
    }
    return true;
}

/**
 * @author: jlagosmu
 * ejecuta la ventana modal de editar
 * @param {type} accion
 * @returns {false}
 */
function closeEditModal(event) {
    //console.log(event.status)
    if (event.status == 'success') {
        document.getElementById('popUpEdit').style.display = "none";
        return false;
    }
}

/**
 * @author: jlagosmu
 * cierra la ventana modal de editar
 * @param {type} accion
 * @returns {false}
 *  */
function openEditModal(event) {
    //console.log(event.status)
    if (event.status == 'success') {
        document.getElementById('popUpEdit').style.display = "block";
        return false;
    }
}

/**
 * @author: jlagosmu
 * limpiasr los input del formualrio principal
 * @param {type} accion
 * @returns {false}
 *  */


function clearForm() {
    var inputs = $('#inputTable :input');
       
    inputs.each(function (index) {
        var input = this;
        input.value = "";
    });
}

/**
 * @author: jlagosmu
 * convierte los datos de un input en matusculas
 * @param {type} accion
 * @returns {false}
 */
function upperCaseLetrers(id) {
    var input = document.getElementById(id);
    var temp = input.value;
    input.value = temp.toUpperCase();
}

function hidenModal(id) {
    document.getElementById(id).style.display = "none";
}

function openDeleteModal(event) {
    if (event.status === 'success') {
        document.getElementById('popUpDelete').style.display = "block";
    }
    return true;
}

